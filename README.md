# fuNTRp - function Neutral/Toggle/Rheostat predictor

A Random Forest-based predictor to classify protein positions by type based on the expected range of mutational impacts at that position: Neutral (most mutations have no or weak effects), Rheostat (range of effects; i.e. func-tional tuning), or Toggle (mostly strong effects).

# Requirements
- Docker (https://www.docker.com)
- Sequence database (RostLab Big_80 database, size: 27.27 GB; http://www.rostlab.org/services/ppmi/download_file?format=gzip&file_to_download=db); A Docker image of PredictProtein (bromberglab/predictprotein) will be downloaded automatically.

# Usage

## 1 Get sources
Open a terminal and checkout the funtrp repository:
```
git clone https://git@bitbucket.org/bromberglab/funtrp.git
```
or download the zipped version:
```
curl --remote-name https://bitbucket.org/bromberglab/funtrp/get/master.zip
unzip master.zip
```
The following steps assume the current working directory as the funtrp (repository) base directory.

## 2 Sequence Database
Download and extract the [sequence database](http://www.rostlab.org/services/ppmi/download_file?format=gzip&file_to_download=db). Move files located in *data/big/* and starting with *big_80* into the *db/* folder of the funtrp base directory; any other databases in the *data/* directory are not required and can be deleted. Open a terminal, navigate to the funtrp base directory and execute:
```
cd db/
wget -O rostlab-data.txz "http://www.rostlab.org/services/ppmi/download_file?format=gzip&file_to_download=db"
xz -d rostlab-data.txz
tar xvf rostlab-data.tar
mv data/big/big_80* .
rm -rf data rostlab-data.tar
```

## 3 Docker image
Docker images are available via the Docker cloud or can be build from the sources available in this repository.

### 3.1 Use available docker images
Open a terminal and execute:
```
docker pull bromberglab/predictprotein
docker pull bromberglab/funtrp
```
### 3.2 Build funtrp docker image from sources
Open a terminal, navigate to the funtrp base directory and execute:
```
docker build -t bromberglab/funtrp .
```

## 4 Run funtrp
funtrp can be run using the provided binary (*funtrp.sh* in the base directory) or directly via the docker engine.

### 4.1 Run funtrp using available binary
Open a terminal and execute:
```
<ABSOLUTE_PATH_TO_FUNTRP_BASE>/funtrp.sh <PATH_TO_QUERY_FASTA_FILE>
```

### 4.2 Run funtrp using docker engine directly
Copy a query sequence in fasta format (<QUERY_ID>.fa or <QUERY_ID>.fasta)to the *mnt/* folder in the funtrp base directory (note here, that <QUERY_FILE_NAME> refers to the filename without any path information and <QUERY_ID> refers to the query file name without extension). Then open a terminal, navigate to the funtrp base directory and execute:
```
docker run --rm \
        -v <ABSOLUTE_PATH_TO_FUNTRP_BASE>/db:/usr/share/rostlab-data/data/big \
        -v <ABSOLUTE_PATH_TO_FUNTRP_BASE>/mnt:/mnt/local-storage \
        bromberglab/predictprotein predictprotein \
        --target=query.profRdb --target=query.mdisorder --target=consurf \
        --output-dir=/mnt/local-storage/pprotein/<QUERY_ID> --work-dir=/mnt/local-storage/pprotein/<QUERY_ID>/tmp \
        --seqfile=/mnt/local-storage/<QUERY_FILE_NAME> \
        --num-cpus=2 \
        --blast-processors=10

docker run --rm \
        -v <ABSOLUTE_PATH_TO_FUNTRP_BASE>/db:/rheostat/db \
        -v <ABSOLUTE_PATH_TO_FUNTRP_BASE>/mnt:/mnt bromberglab/funtrp <QUERY_FILE_NAME>
```

## 5 Example
Open a terminal, navigate to the funtrp base directory and execute:
```
./funtrp.sh example/P37173.fasta
```
Results will then be available at *mnt/P37173_funtrp.csv*
