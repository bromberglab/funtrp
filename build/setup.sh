#!/usr/bin/env bash

##-------------------------------------------------------
# UPDATE CONFIG FILES
##-------------------------------------------------------

# set UTF-8 environment
echo 'LC_ALL=en_US.UTF-8' >> /etc/environment
echo 'LANG=en_US.UTF-8' >> /etc/environment
echo 'LC_CTYPE=en_US.UTF-8' >> /etc/environment

# setup perlbrew
export PERLBREW_ROOT=/usr/share/profphd/prof/perl5
export PERLBREW_HOME=/tmp/.perlbrew
perlbrew init
source /usr/share/profphd/prof/perl5/etc/bashrc
perlbrew install -j `nproc` --notest --noman perl-5.10.1
# rename system perl binary
mv /usr/bin/perl /usr/bin/perl_system

# link correct old version installed previously instead
ln -s /usr/share/profphd/prof/perl5/perls/perl-5.10.1/bin/perl /usr/bin/perl

# set environment variables for library accordingly
export PERL5LIB=/usr/share/perl5/

# setup Python3
# pip3 install --upgrade pip
pip3 install --upgrade setuptools
pip3 install numpy
pip3 install pandas
pip3 install biopython
pip3 install rpy2
pip3 install python-weka-wrapper3
pip3 install tzlocal

# setup R
cd /rheostat
Rscript setup.R

# install BLAST+ 2.6.0
cd /rheostat
wget -nv ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.6.0/ncbi-blast-2.6.0+-x64-linux.tar.gz
tar xzvf ncbi-blast-2.6.0+-x64-linux.tar.gz
rm -f ncbi-blast-2.6.0+-x64-linux.tar.gz

# install sift 4.0.3b
cd /rheostat
wget http://sift.jcvi.org/www/sift4.0.3b.tar.gz
tar xzvf sift4.0.3b.tar.gz
rm -f sift4.0.3b.tar.gz

# install mafft 7.310
cd /rheostat
wget http://mafft.cbrc.jp/alignment/software/mafft-7.310-with-extensions-src.tgz
tar xzvf mafft-7.310-with-extensions-src.tgz
rm -f mafft-7.310-with-extensions-src.tgz
cd /rheostat/mafft-7.310-with-extensions/core/
make clean
make
make install
