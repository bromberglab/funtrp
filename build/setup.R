# setup R environment

# set default repository
options(repos=structure(c(CRAN="https://cloud.r-project.org/")))

# install packages and dependencies
install.packages("plotly")
install.packages("XML")
install.packages("pbapply")
install.packages("flux")
install.packages('/rheostat/bromberglab_suite/R/bblsuite', repos=NULL, type="source")
