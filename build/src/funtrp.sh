#!bin/bash

export PYTHONPATH=$PYTHONPATH:/rheostat/bromberglab_suite/python

queryfile=$(basename -- "$1")
extension="${queryfile##*.}"
queryid="${queryfile%.*}"
echo "Running fuNTRP for: $1"
mkdir "/mnt/${queryid}_out/"
cp /mnt/$1 "/mnt/${queryid}_out/"
python3.5 -m bblsuite.protein.rheostat.pipeline_new "/mnt/${queryid}_out/$1" -p -e -d -m RF -l /rheostat/models -o "/mnt/${queryid}_out/"
cp "/mnt/${queryid}_out/${queryid}.TvsNvsR.predictions.csv" "/mnt/${queryid}_funtrp.csv"
echo "---------------------------------------------------------------------------------"
echo "Prediction results saved at: ${queryid}_funtrp.csv"
echo "---------------------------------------------------------------------------------"
