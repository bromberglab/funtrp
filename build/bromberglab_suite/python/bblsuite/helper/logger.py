#!/usr/bin/env python3
import sys, logging
from bblsuite.helper import globals, tools
from bblsuite.helper.config import Config

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class Logger:

    @staticmethod
    def get_logger(name=None, level=None, file=None, console=False):
        """Create custom logger at given level with optional file handler"""

        # create logger instance
        # to only get name or actual creator class use: logger = logging.getLogger(__name__)
        if not name:
            name = tools.caller_name()
        logger = logging.getLogger(name)

        # only create logger if not instance available for specific class
        if logger.handlers.__len__() == 0:

            # if config has logging directives use those and overwrite current settings
            config = Config()
            if config.config.has_section('Logging'):
                if file is None:
                    if config.has_property('Logging', 'file'):
                        file = config.get_property('Logging', 'file')
                    else:
                        file = config.DEFAULT_LOG_LEVEL
                if not level:
                    level = config.DEFAULT_LOG_FILE
                    if config.has_property('Logging', 'level'):
                        level = config.get_property('Logging', 'level')

            if not globals.QUIET:
                # set logging level
                logger.setLevel(config.DEFAULT_LOG_LEVEL)

                # create a logging format
                formatter = CustomFormatter()

                # create a stream handler
                if console or not file:
                    stream_handler = logging.StreamHandler(sys.stdout)
                    stream_handler.setFormatter(formatter)
                    logger.addHandler(stream_handler)

                # create a file handler
                if file:
                    file_handler = logging.FileHandler(file)
                    file_handler.setFormatter(formatter)
                    logger.addHandler(file_handler)

                logger.debug('Logger attached')
            else:
                # set logging level to error
                logger.setLevel('ERROR')

        return logger

class CustomFormatter(logging.Formatter):
    """Custom log output formatter"""

    dbg_fmt = '%(asctime)s [ %(levelname)s ] (%(name)s) : %(message)s'
    info_fmt = '%(asctime)s [ %(levelname)s ] (%(name)s) : %(message)s'
    warn_fmt = '%(asctime)s [ %(levelname)s ] (%(name)s{%(module)s}) : %(message)s'
    err_fmt = '%(asctime)s [ %(levelname)s ] (%(name)s{%(module)s})[%(lineno)d] : %(message)s'

    def __init__(self, fmt="X%(levelno)s: %(msg)s"):
        logging.Formatter.__init__(self, fmt)

    def format(self, record):
        """Re-format log messages"""

        # Save the original format configured by the user
        # when the logger formatter was instantiated
        format_orig = self._fmt

        # Replace the original format with one customized by logging level

        if record.levelno == logging.DEBUG:
            self._fmt = CustomFormatter.dbg_fmt

        elif record.levelno == logging.INFO:
            self._fmt = CustomFormatter.info_fmt

        elif record.levelno == logging.WARNING:
            self._fmt = CustomFormatter.warn_fmt

        elif record.levelno == logging.ERROR:
            self._fmt = CustomFormatter.err_fmt

        # Python 3 needs the following assignment due to changes in internal logging machanism
        self._style = logging.PercentStyle(self._fmt)

        # Call the original formatter class to do the grunt work
        result = logging.Formatter.format(self, record)

        # Restore the original format configured by the user
        self._fmt = format_orig

        return result

print = Logger.get_logger("print", level="INFO").info
