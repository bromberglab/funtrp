#!/usr/bin/env python3
import os
import inspect
import errno
import signal
import random
import string
from functools import wraps

"""
Collection of various tools ands methods

"""

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


def caller_name(skip=2, function=False):
    """Get a name of a caller in the format module.class.method

       `skip` specifies how many levels of stack to skip while getting caller
       name. skip=1 means "who calls me", skip=2 "who calls my caller" etc.

       An empty string is returned if skipped levels exceed stack height
    """
    stack = inspect.stack()
    start = 0 + skip
    if len(stack) < start + 1:
        return ''
    parentframe = stack[start][0]

    name = []
    module = inspect.getmodule(parentframe)
    # `modname` can be None when frame is executed directly in console
    # TODO consider using __main__
    if module:
        name.append(module.__name__)
    # detect classname
    if 'self' in parentframe.f_locals:
        # I don't know any way to detect call from the object method
        # XXX: there seems to be no way to detect static method call - it will
        #      be just a function call
        name.append(parentframe.f_locals['self'].__class__.__name__)
    codename = parentframe.f_code.co_name
    if function and codename != '<module>':  # top level usually
        name.append(codename)  # function or a method
    del parentframe
    return ".".join(name)


class TimeoutError(Exception):
    pass


def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wraps(func)(wrapper)

    return decorator


def get_random_string(length=16):
    return ''.join([random.choice(string.ascii_letters + string.digits) for n in range(length)])


def find_fasta_in_dir(root_dir):
    found_files = []
    for root, directories, filenames in os.walk(root_dir):
        for filename in filenames:
            if filename[0] == '.':
                continue
            elif filename.endswith(".fasta") or filename.endswith(".fa") or filename.endswith(".fna") or filename.endswith(".fastq") or filename.endswith(".fq"):
                found_files.append(os.path.join(root, filename))
    return found_files

def is_subseq(x, y):
    it = iter(y)
    return all(c in it for c in x)
