#!/usr/bin/python
import MySQLdb
from bblsuite.helper import config, logger

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class MySQLHandler:
    """Communication with a MySQL database"""

    log = logger.Logger.get_logger()

    def __init__(self, db_from_config=None):
        self.conf = config.Config()
        if db_from_config is None:
            self.alias = None
            self.conn = None
            self.hostname = None
            self.username = None
            self.password = None
            self.database = None
            self.port = None
        else:
            self.alias = db_from_config.lower()
            self.get_database_config()
        self.connected = False
        self.time_zone = self.conf.get_property('clubber', 'timezone')

    def duplicate(self):
        d = MySQLHandler()
        d.conn = None
        d.alias = self.alias
        d.set_database(self.hostname, self.username, self.password, self.database, self.port)
        return d

    def set_database(self, hostname, username, password, database=None, port=3306):
        """Manually set database"""

        self.alias = hostname
        self.hostname = hostname
        self.username = username
        self.password = password
        self.database = database
        self.port = port

    def get_database_config(self):
        """
        Create database from config.

        Args:

        Returns:
            none
        """

        if self.alias == 'default':
            self.alias = self.conf.get_property('General', 'default.db')
        elif self.alias == 'default-clubber':
            self.alias = self.conf.get_property('clubber', 'database')
        if not self.conf.has_property('Database', '%s.hostname' % self.alias):
            self.log.warn('No config found for database identifier: %s' % self.alias)
        else:
            self.hostname = self.conf.get_property('Database', '%s.hostname' % self.alias)
            self.database = self.conf.get_property('Database', '%s.database' % self.alias)
            self.port = int(self.conf.get_property('Database', '%s.port' % self.alias))
            self.username = self.conf.get_property('Database', '%s.username' % self.alias)
            self.password = self.conf.get_property('Database', '%s.password' % self.alias)

    def connect(self):
        """Connect to mysql db"""

        if self.connected:
            self.log.debug('database already connected')
        else:
            self.conn = MySQLdb.connect(
                host=self.hostname, user=self.username, passwd=self.password, db=self.database, port=self.port
            )
            self.log.debug('Opened connection to database: %s (%s)' % (self.alias, self.hostname))

            self.query('SET time_zone="%s"' % self.time_zone)
            self.connected = True

    def disconnect(self):
        """Close mysql connection"""

        if self.connected:
            self.conn.close()
            self.log.debug('Closed connection to database: %s (%s)' % (self.alias, self.hostname))
            self.connected = False

    def query(self, mysql, args=None):
        """Submit a single mysql query"""

        try:
            db_cursor = self.conn.cursor()
            # return_code =
            if not args:
                db_cursor.execute(mysql)
            else:
                db_cursor.execute(mysql, args)
            resultset = db_cursor.fetchall()
            db_cursor.close()
            return resultset
        except MySQLdb.OperationalError as e:
            self.connected = False
            self.connect()
            self.log.warn('Operational error: reconnecting and trying again...')
            self.query(mysql)
        except MySQLdb.InterfaceError as e:
            self.connected = False
            self.connect()
            self.log.warn('Interface error: reconnecting and trying again...')
            self.query(mysql)

    def insert(self, mysql):
        """Submit a single insert query"""

        db_cursor = self.conn.cursor()
        # return_code =
        db_cursor.execute(mysql)
        resultset = db_cursor.fetchall()
        id = db_cursor.lastrowid
        db_cursor.close()
        return id

    def update(self, mysql):
        """Submit a single update query"""

        db_cursor = self.conn.cursor()
        # return_code =
        db_cursor.execute(mysql)
        resultset = db_cursor.fetchall()
        db_cursor.close()
        return resultset

    def query_multi(self, mysql_list):
        """Submit multiple mysql queries"""

        resultsets = []
        db_cursor = self.conn.cursor()
        for mysql in mysql_list:
            # return_code =
            db_cursor.execute(mysql)
            resultsets += [db_cursor.fetchall()]
        db_cursor.close()
        return resultsets

    def query_multi_values(self, mysql_base, value_list):
        """Insert multiple value tuples"""

        cmd_combined = '%s ' % mysql_base
        db_cursor = self.conn.cursor()
        for val in value_list[0:-1]:
            cmd_combined += '%s,' % val
        cmd_combined += '%s' % value_list[-1]
        # return_code =
        db_cursor.execute(cmd_combined)
        resultset = db_cursor.fetchall()
        db_cursor.close()
        return resultset
