#!/usr/bin/env python3

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


class Aminoacid:

    def __init__(self, one_letter_code):
        self.oneLetterCode = one_letter_code

    alphabet = ['A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y', '*']
    alphabet_extended = ['X']

    codontable = {
        'ATA': 'I', 'ATC': 'I', 'ATT': 'I', 'ATG': 'M',
        'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACT': 'T',
        'AAC': 'N', 'AAT': 'N', 'AAA': 'K', 'AAG': 'K',
        'AGC': 'S', 'AGT': 'S', 'AGA': 'R', 'AGG': 'R',
        'CTA': 'L', 'CTC': 'L', 'CTG': 'L', 'CTT': 'L',
        'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCT': 'P',
        'CAC': 'H', 'CAT': 'H', 'CAA': 'Q', 'CAG': 'Q',
        'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGT': 'R',
        'GTA': 'V', 'GTC': 'V', 'GTG': 'V', 'GTT': 'V',
        'GCA': 'A', 'GCC': 'A', 'GCG': 'A', 'GCT': 'A',
        'GAC': 'D', 'GAT': 'D', 'GAA': 'E', 'GAG': 'E',
        'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGT': 'G',
        'TCA': 'S', 'TCC': 'S', 'TCG': 'S', 'TCT': 'S',
        'TTC': 'F', 'TTT': 'F', 'TTA': 'L', 'TTG': 'L',
        'TAC': 'Y', 'TAT': 'Y', 'TAA': '_', 'TAG': '_',
        'TGC': 'C', 'TGT': 'C', 'TGA': '_', 'TGG': 'W',
    }

    possiblesnptable = {
        'A': ['A', 'D', 'E', 'G', 'P', 'S', 'T', 'V'],
        'C': ['C', 'F', 'G', 'R', 'S', 'W', 'Y'],
        'D': ['A', 'D', 'E', 'G', 'H', 'N', 'V', 'Y'],
        'E': ['A', 'D', 'E', 'G', 'K', 'Q', 'V'],
        'F': ['C', 'F', 'I', 'L', 'S', 'V', 'Y'],
        'G': ['A', 'C', 'D', 'E', 'G', 'R', 'S', 'V', 'W'],
        'H': ['D', 'H', 'L', 'N', 'P', 'Q', 'R', 'Y'],
        'I': ['F', 'I', 'K', 'L', 'M', 'N', 'R', 'S', 'T', 'V'],
        'K': ['E', 'I', 'K', 'M', 'N', 'Q', 'R', 'T'],
        'L': ['F', 'H', 'I', 'L', 'M', 'P', 'Q', 'R', 'S', 'V', 'W'],
        'M': ['I', 'K', 'L', 'M', 'R', 'T', 'V'],
        'N': ['D', 'H', 'I', 'K', 'N', 'S', 'T', 'Y'],
        'P': ['A', 'H', 'L', 'P', 'Q', 'R', 'S', 'T'],
        'Q': ['E', 'H', 'K', 'L', 'P', 'Q', 'R'],
        'R': ['C', 'G', 'H', 'I', 'K', 'L', 'M', 'P', 'Q', 'R', 'S', 'T', 'W'],
        'S': ['A', 'C', 'F', 'G', 'I', 'L', 'N', 'P', 'R', 'S', 'T', 'W', 'Y'],
        'T': ['A', 'I', 'K', 'M', 'N', 'P', 'R', 'S', 'T'],
        'V': ['A', 'D', 'E', 'F', 'G', 'I', 'L', 'M', 'V'],
        'W': ['C', 'G', 'L', 'R', 'S', 'W'],
        'Y': ['C', 'D', 'F', 'H', 'N', 'S', 'Y']
    }

    propertymapping = {
        1: ('o', 'hydrophobic'),
        2: ('i', 'hydrophilic'),
        3: ('a', 'aromatic'),
        4: ('c', 'non-polar aliphatic'),
        5: ('s', 'small'),
        6: ('u', 'polar uncharged'),
        7: ('n', 'negatively charged / acidic'),
        8: ('p', 'positively charged / basic')
    }

    propertypairs = {
        (1,2): -1,
        (3,4): -1,
        (4,6): -1,
        (6,7): -1,
        (6,8): -1,
        (7,8): -1
    }

    propertytable = {
        'A': [1, 4, 5],
        'C': [2, 5, 6],
        'D': [2, 7],
        'E': [2, 7],
        'F': [1, 3],
        'G': [2, 4, 5],
        'H': [2, 8],
        'I': [1, 4],
        'K': [2, 8],
        'L': [1, 4],
        'M': [1, 4],
        'N': [2, 6],
        'P': [1, 4],
        'Q': [2, 6],
        'R': [2, 8],
        'S': [2, 5, 6],
        'T': [2, 6],
        'V': [1, 4],
        'W': [1, 3],
        'Y': [1, 3]
    }

    def get_coding_triplets(self):
        """Returns all coding triplets of the amino acid"""
    
        codons = []
        for codon, aa in self.codontable.items():
            if self.oneLetterCode == aa:
                codons += [codon]
        return codons

    def get_possible_snps(self):
        """Returns all possible SNP amino acids of the amino acid"""

        if self.oneLetterCode in Aminoacid.alphabet_extended:
            return []
        return self.possiblesnptable[self.oneLetterCode]

    def get_basic_properties(self):
        """Returns amino acids basic properties"""

        if self.oneLetterCode in Aminoacid.alphabet_extended:
            return []

        properties = self.propertytable[self.oneLetterCode]
        properties_mapped = []
        for p in properties:
            properties_mapped.append(Aminoacid.propertymapping[p])
        return properties_mapped

    def get_basic_exchange_scoring(self, aa):
        """Returns simple score regarding exchangeability of amino acids based on basic aa properties"""

        if self.oneLetterCode in Aminoacid.alphabet_extended:
            return 0

        prop_self = self.propertytable[self.oneLetterCode]
        prop_aa = self.propertytable[aa]
        score = 0
        for p1 in prop_self:
            for p2 in prop_aa:
                tuple = (min(p1,p2), max(p1,p2))
                if tuple in self.propertypairs.keys():
                    score += self.propertypairs[tuple]
        return score

    @classmethod
    def get_all_one_letter(cls):
        """Returns a list of all available one letter codes for amino acids.
        Stop codons are represented by '*'"""

        return Aminoacid.alphabet
