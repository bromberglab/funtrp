import os
import re
import subprocess
import multiprocessing
import pandas as pd
import numpy as np
import csv
from rpy2 import robjects
from rpy2.robjects import pandas2ri
from rpy2.robjects.packages import importr
from bblsuite.helper import logger, tools
from Bio.Blast import NCBIXML
from Bio import AlignIO, SeqIO
from Bio.Align import AlignInfo
from bblsuite.protein.alignment.seqlim import MSeq
from bblsuite.protein.aminoacid.aminoacid_main import Aminoacid
from bblsuite.protein.rheostat import predictprotein


BLAST_DB_98 = "/rheostat/db/big_98"
BLAST_DB_80 = "/rheostat/db/big_80"
BLAST_DB = BLAST_DB_80
SNAP2_DB = "/rheostat/data/datasets_selected/snap2"
BLAST = "/rheostat/ncbi-blast-2.6.0+/bin/blastp"
PSIBLAST = "/rheostat/ncbi-blast-2.6.0+/bin/psiblast"
BLAST_FORMATTER = "/rheostat/ncbi-blast-2.6.0+/bin/blast_formatter"
PYTHON = "python3"
BLAST2SAF = "/usr/share/librg-utils-perl/blast2saf.pl"
SAF2HSSP = "/usr/share/librg-utils-perl/copf.pl"
HSSPFILTER = "/usr/share/librg-utils-perl/hssp_filter.pl"
PROFBVAL = "/usr/bin/profbval"
PROF = "/usr/bin/prof"
EXPORT = "PERL5LIB=/usr/share/perl5/"
RATE4SITE = "rate4site"
CONSCALC = '/rheostat/jsdiv/score_conservation_brlab.py'
EVOTRACE = '/rheostat/evotrace/wetc'
CSVTOARFF = '/rheostat/scripts/csv2arff.sh'

MODEL_R_TRAIN = '/rheostat/models/modelR_raw.arff'
MODEL_T_TRAIN = '/rheostat/models/modelT_raw.arff'
MODEL_N_TRAIN = '/rheostat/models/modelN_raw.arff'
MODEL_4_TRAIN = '/rheostat/models/model4_raw.arff'

MODEL_R = '/rheostat/models/modelR.model'
MODEL_T = '/rheostat/models/modelT.model'
MODEL_N = '/rheostat/models/modelN.model'
MODEL_4 = '/rheostat/models/model4.model'

Msingle_RUN = '/rheostat/scripts/single_models.sh'
M4_RUN = '/rheostat/scripts/model4.sh'

log = logger.Logger.get_logger(console=False)
cpu_count = multiprocessing.cpu_count()

query_sequence_obj = None

datasets_wt = {
    'lacI': 1,
    'set1': 1,
    'set3': 1,
    'set7': 1.06,
    'set8': 1,
    'set12': 3.719,
    'set13': 1,
    'set15': 1,
    'set17': 1,
    'set24': 0.98
}

datasets_severe = {
    'lacI': 1820,
    'set1': 0,
    'set3': 0.06,
    'set7': -1.75,
    'set8': 0.1,
    'set12': 3,
    'set13': 0.6,
    'set15': 0.38,
    'set17': 0.1,
    'set24': 1.15
}

pprotein_parser = predictprotein.Parser()


def conservation(fasta_in, method, force_run=False):
    if method == "rate4site":
        return rate4site_wrapper(fasta_in, force_run)
    elif method == "consCalc":
        return conscalc_wrapper(fasta_in, force_run)
    elif method == "evotrace":
        return evotrace_wrapper(fasta_in, force_run)
    else:
        log.warn('conservation: method not known [%s]' % method)


def psiblast_run(fasta_in, cmd_extension, suffix=".psiblast", fasta_eval_threshold=None, force_run=False):
    # psiblastp run
    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    psiblast_out_txt = os.path.join(basepath, filename + suffix + ".blast")
    psiblast_out_archive = os.path.join(basepath, filename + suffix + ".asn")
    psiblast_out_reformat = os.path.join(basepath, filename + suffix + ".legacy")
    psiblast_out_xml = os.path.join(basepath, filename + suffix + ".xml")
    psiblast_out_fasta = os.path.join(basepath, filename + suffix + ".fasta")
    if not os.path.isfile(psiblast_out_archive) or force_run:
        psiblast_wrapper(fasta_in, cmd_extension, suffix)
    else:
        log.info('psiblast: skipping run')
    blast_reformat(psiblast_out_txt, psiblast_out_reformat)
    # log.info('psiblast: extracting fasta fos msa from predictprotein run...')
    # psiblast_out_xml_pprotein = os.path.join(os.path.join(PPROTEIN_DB, filename), "query" + ".profb4snap")
    blast_extract_fasta(fasta_in, psiblast_out_xml, psiblast_out_fasta, evalue_threshold=fasta_eval_threshold)


def prof_wrapper(fasta_in, force_run=False):
    log.info("prof: executing...")
    env_wd = os.getcwd()

    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    prof_file = os.path.join(basepath, filename + ".prof")
    # rdbprof_file = os.path.join(basepath, filename + ".rdbProf")
    prof_parsed_out = os.path.join(basepath, filename + "_prof_parsed.csv")

    if not os.path.isfile(prof_file) or force_run:
        os.chdir(basepath)
        subprocess.call('%s %s both' % (PROF, fasta_in), shell=True)
    else:
        log.info('prof: skipping run')

    log.info('prof: parsing...')
    with open(prof_file) as csv_in:
        # No	AA	PHEL	RI_S	PACC	PREL	RI_A	pH	pE	pL	Pbe	Pbie	OtH	OtE	OtL	Ot0	Ot1	Ot2	Ot3	Ot4	Ot5	Ot6	Ot7	Ot8	Ot9
        df = pd.read_csv(
            csv_in,
            sep='\t',
            comment="#",
            usecols=['No', 'AA', 'PHEL', 'RI_S', 'PACC', 'PREL', 'RI_A', 'pH', 'pE', 'pL', 'Pbe', 'Pbie',
                     'OtH', 'OtE', 'OtL', 'Ot0', 'Ot1', 'Ot2', 'Ot3', 'Ot4', 'Ot5', 'Ot6', 'Ot7', 'Ot8', 'Ot9']
        )
        df.rename(columns={'No': 'position'}, inplace=True)
        df.rename(columns={'AA': 'WT_aa'}, inplace=True)
        df = df.set_index('position')
        df.index = pd.to_numeric(df.index)
        output_file = open(prof_parsed_out, 'w')
        df.to_csv(output_file)
        output_file.close()

        # FILTER FEATURES
        df = df.drop(['PHEL', 'RI_S', 'PREL', 'RI_A', 'Pbe', 'Pbie', 'Ot0', 'Ot1', 'Ot2', 'Ot3', 'Ot4', 'Ot5', 'Ot6', 'Ot7',
                 'Ot8', 'Ot9'], axis=1)

    os.chdir(env_wd)

    return df


def profbval_wrapper(fasta_in, suffix=".psiblast", force_run=False):
    log.info("profbval: executing...")

    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    psiblast_out_reformat = os.path.join(basepath, filename + suffix + ".legacy")
    bval_out = os.path.join(basepath, filename + ".bval")

    if not os.path.isfile(bval_out) or force_run:
        # hssp conversion-saf file
        log.info("profbval: blast to saf...")
        saf_out = os.path.join(basepath, filename + ".saf")
        subprocess.call('%s fasta=%s eSaf=1 saf=%s %s' % (BLAST2SAF, fasta_in, saf_out, psiblast_out_reformat), shell=True)
        if not os.path.isfile(saf_out):
            log.warn("profbval: BLAST2SAF error!")

        # hssp converison-saf to hssp
        log.info("profbval: saf to hssp...")
        hssp_out = os.path.join(basepath, filename + ".hssp")
        subprocess.call('%s %s formatIn=saf formatOut=hssp fileOut=%s exeConvertSeq=convert_seq' % (SAF2HSSP, saf_out, hssp_out), shell=True)
        if not os.path.isfile(hssp_out):
            log.warn("profbval: SAF2HSSP error!")

        # filter hssp
        log.info("profbval: filter hssp...")
        filtered_hssp_out = os.path.join(basepath, filename + ".filtered_hssp")
        subprocess.call('%s red=80 %s fileOut=%s' % (HSSPFILTER, hssp_out, filtered_hssp_out), shell=True)
        if not os.path.isfile(filtered_hssp_out):
            log.warn('profbval: HSSP filter error!')
            log.debug('%s red=80 %s fileOut=%s' % (HSSPFILTER, hssp_out, filtered_hssp_out))

        # run prof to create rdbProf file
        log.info("profbval: running prof (rdbProf)...")
        rdbProf_out = os.path.join(basepath, filename + ".rdbProf")
        cmd = '%s %s both numresMin=17 nresPerLineAli=60 riSubSec=4 riSubAcc=3 fileRdb=%s' % (PROF, filtered_hssp_out, rdbProf_out)
        subprocess.call(cmd, shell=True)
        if not os.path.isfile(rdbProf_out):
            log.warn('profbval: rdbProf error!')

        # run profbval
        log.info("profbval: running profbval...")
        subprocess.call('%s %s %s %s %s 9 0' % (PROFBVAL, fasta_in, rdbProf_out, filtered_hssp_out, bval_out), shell=True)
        if not os.path.isfile(bval_out):
            log.warn('profbval: profbval error!')
    else:
        log.info('profbval: skipping run')

    log.info("profbval: parsing...")
    bval_parsed_out = os.path.join(basepath, filename + ".bval_parsed.csv")
    with open(bval_out) as new_csv_in:
        df = pd.read_csv(new_csv_in, sep='\t', usecols=['number', 'residue', 'accessibility', 'Bnorm'])
        df.rename(columns={'number': 'position'}, inplace=True)
        df.rename(columns={'residue': 'WT_aa'}, inplace=True)
        df = df.set_index('position')
        df.index = pd.to_numeric(df.index)
        output_file = open(bval_parsed_out, 'w')
        df.to_csv(output_file)
        output_file.close()

    # FILTER FEATURES
    df = df.drop(['acc'], axis=1)

    return df


def tmseg_wrapper(fasta_in, suffix=".psiblast", force_run=False):
    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)

    pssm = os.path.join(basepath, filename + suffix + ".pssm")
    tmseg_out = os.path.join(basepath, filename + ".tmseg.out")
    tmseg_raw = os.path.join(basepath, filename + ".tmseg.raw")
    tmseg_raw_parsed = os.path.join(basepath, filename + ".tmseg.raw.parsed.csv")

    if not os.path.isfile(tmseg_raw) or force_run:
        log.info("tmseg: executing...")
        cmd = 'tmseg -i %s -p %s -o %s -r %s' % (fasta_in, pssm, tmseg_out, tmseg_raw)
        subprocess.call(cmd, shell=True)
    else:
        log.info('tmseg: skipping run')

    log.info("tmseg: parsing...")
    with open(tmseg_raw) as csv_in:
        # SEQ	SOL	TMH	SIG	SEG	TOP	PRED
        df = pd.read_csv(
            csv_in,
            sep='\t',
            comment="#",
            header=None,
            names=['SEQ', 'SOL', 'TMH', 'SIG', 'SEG', 'TOP', 'PRED']
        )
        df.rename(columns={'SEQ': 'WT_aa'}, inplace=True)
        #df = df.set_index('position')
        df.index = np.arange(1, len(df.index) + 1)
        df.index.name = 'position'
        output_file = open(tmseg_raw_parsed, 'w')
        df.to_csv(output_file)
        output_file.close()

    # FILTER FEATURES
    df = df.drop(['SOL', 'TMH', 'SIG', 'PRED'], axis=1)

    return df


def mafft_wrapper(fasta_in, suffix=".psiblast", force_run=False):
    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    fasta_sequences_in = os.path.join(basepath, filename + suffix + ".fasta")

    # mafft run
    mafft_out = os.path.join(basepath, filename + ".mafft")
    mafft_out_clustal = os.path.join(basepath, filename + ".mafft.msf")

    if not os.path.isfile(mafft_out) or force_run:
        log.info("mafft: running mafft... (using %i cores)" % cpu_count)
        cmd = 'mafft --retree 2 --thread %i --quiet %s > %s' % (cpu_count, fasta_sequences_in, mafft_out)
        subprocess.call(cmd, shell=True)
    else:
        log.info('mafft: skipping run')

    input_handle = open(mafft_out, "r")
    mseq = MSeq.parse(input_handle, 'fasta')
    input_handle.close()
    output_handle = open(mafft_out_clustal, "w")
    mseq.write(output_handle, 'msf', max_tag_len=30)
    output_handle.close()

    if not os.path.isfile(mafft_out):
        log.warn('mafft: mafft error!')

    return mafft_out


def blast_reformat(input, output):
    outfile = open(output, 'w')

    query_flag = False

    with open(input, 'r') as infile:
        for line in infile:
            parsed = line

            if not query_flag and line.startswith('Query'):
                query_flag = True
                line = next(infile)
                while not line.strip().startswith('Length'):
                    line = next(infile)
                length = line.strip().split("=")[1]
                parsed += "(letters %s)\n\nSearching..................................................done\n\n" % length

            if line[0] == '>':
                if line[1] == ' ':
                    parsed = '>' + parsed[2:]
            else:
                if line[0:6] == "Query ":
                    parsed = "Query:" + parsed[6:]
                elif line[0:6] == "Sbjct ":
                    parsed = "Sbjct:" + parsed[6:]

            outfile.write(parsed)

    outfile.close()


def rate4site_wrapper(fasta_in, force_run=False):
    """Calculate conservation scores using rate4site"""
    env_wd = os.getcwd()

    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    msa_in = os.path.join(basepath, filename + ".mafft")
    wd = os.path.join(basepath, filename + "_rate4site")
    if not os.path.exists(wd):
        os.mkdir(wd)
    rate4site_out = os.path.join(wd, filename + ".rate4site")
    rate4site_out_parsed = os.path.join(basepath, filename + ".rate4site.parsed.csv")

    conservation = {}

    if not os.path.isfile(rate4site_out) or force_run:
        log.info("rate4site: calculating conservation...")
        os.chdir(wd)
        # bash_command_rate4site = '%s -o %s -s %s' % (RATE4SITE, rate4site_out, msa_in)
        # subprocess.call(bash_command_rate4site, shell=True)
    else:
        log.info('rate4site: skipping run')

    log.info("rate4site: parsing...")

    with open(rate4site_out) as reader:
        new_list = []
        for line in reader:
            if line[0] == '#':
                continue
            re_split = re.findall('([A-Z]|\d+/\d+|\d*\.\d+|-\d*\.\d+|\d+|-\d+)', line)
            if re_split:
                new_list.append([re_split[0], re_split[1], re_split[2], ','.join(re_split[3:5]), re_split[5], re_split[6]])

        df = pd.DataFrame(new_list, columns=['POS', 'SEQ', 'SCORE', 'QQ-INTERVAL', 'STD', 'MSA DATA'])

        df.rename(columns={'POS': 'position'}, inplace=True)
        df.rename(columns={'SEQ': 'WT_aa'}, inplace=True)
        df = df.set_index('position')
        df.index = pd.to_numeric(df.index)
    output_file = open(rate4site_out_parsed, 'w')
    df.to_csv(output_file)
    output_file.close()

    os.chdir(env_wd)

    return df


def conscalc_wrapper(fasta_in, force_run=False):
    """Calculate conservation scores using conscalc"""

    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    msa_in = os.path.join(basepath, filename + ".mafft")
    conscalc_out = os.path.join(basepath, filename + ".consCalc")
    conscalc_out_parsed = os.path.join(basepath, filename + ".consCalc.parsed.csv")

    conservation = {}

    if not os.path.isfile(conscalc_out) or force_run:
        log.info("conscalc: executing...")
        query = read_query_sequence(fasta_in)
        bash_command_cons_calc = '%s -a "%s" -o %s %s' % (CONSCALC, query.id, conscalc_out, msa_in)
        subprocess.call(bash_command_cons_calc, shell=True)
    else:
        log.info('conscalc: skipping run')

    log.info("conscalc: parsing...")

    with open(conscalc_out) as csv_in:
        # align_column_number	amino acid	score
        df = pd.read_csv(
            csv_in,
            sep='\t',
            comment="#",
            header=None,
            names=['amino acid', 'score']
        )
        # df.rename(columns={'align_column_number': 'position'}, inplace=True)
        df.rename(columns={'amino acid': 'WT_aa'}, inplace=True)
        # df = df.set_index('position')
        df.index = np.arange(1, len(df.index) + 1)
        df.index.name = 'position'
        output_file = open(conscalc_out_parsed, 'w')
        df.to_csv(output_file)
        output_file.close()

    return df


def evotrace_wrapper(fasta_in, force_run=False):
    """Calculate conservation scores using wetc (Evolutionary Trace Server)"""
    env_wd = os.getcwd()

    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    wd = os.path.join(basepath, filename + "_evotrace")
    if not os.path.exists(wd):
        os.mkdir(wd)
    msa_in = os.path.join(basepath, filename + ".mafft.msf")

    res = {}

    os.chdir(wd)
    query = read_query_sequence(fasta_in)
    id = (query.id).split('|')[-1]
    evotrace_out = os.path.join(wd, id + '.ranks')
    evotrace_out_parsed = os.path.join(basepath, filename + '.evotrace.parsed.csv')

    if not os.path.exists(evotrace_out) or force_run:
        log.info("evotrace: executing...")
        bash_command_wetc = '%s -p %s -x "%s" -o "%s" > /dev/null' % (EVOTRACE, msa_in, id, id)
        subprocess.call(bash_command_wetc, shell=True)
    else:
        log.info("evotrace: skipping run")

    # parsing
    log.info("evotrace: parsing...")

    # % alignment  # residue#   AA type  coverage     variability_count         variability       Score(rvET)
    with open(evotrace_out) as reader:
        new_list = []
        for line in reader:
            if line[0] == '%':
                continue
            re_split = re.findall('(\d*\.\d+|\d+|[.A-Z]+)', line)
            if re_split:
                new_list.append(
                    [re_split[0], re_split[1], re_split[2], re_split[3], re_split[4], re_split[5], re_split[6]])

        df = pd.DataFrame(new_list, columns=['alignment', 'residue', 'AA type', 'coverage', 'variability_count', 'variability', 'Score(rvET)'])

        df.rename(columns={'residue': 'position'}, inplace=True)
        df.rename(columns={'AA type': 'WT_aa'}, inplace=True)
        df = df.set_index('position')
        df.index = np.arange(1, len(df.index) + 1)
        df.index.name = 'position'
        output_file = open(evotrace_out_parsed, 'w')
        df.to_csv(output_file)
        output_file.close()

    os.chdir(env_wd)

    # FILTER FEATURES
    df = df.drop(['alignment', 'variability_count'], axis=1)

    return df


def blast_wrapper(fasta_in):
    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)

    # blastp run
    log.info("blast: running BLAST... (using %i cores)" % cpu_count)
    blast_out_archive = os.path.join(basepath, filename + ".blast.asn")
    blast_out_file = os.path.join(basepath, filename + ".blast")
    blast_out_xml = os.path.join(basepath, filename + ".blast.xml")

    subprocess.call('%s -db %s -query %s -outfmt 11 -out %s -num_threads %i' % (BLAST, BLAST_DB, fasta_in, blast_out_archive, cpu_count),
        shell=True)
    if os.path.isfile(blast_out_archive):
        subprocess.call('%s -archive %s -outfmt 5 -out %s' % (BLAST_FORMATTER, blast_out_archive, blast_out_xml),
                        shell=True)
        subprocess.call('%s -archive %s -outfmt 0 -out %s' % (BLAST_FORMATTER, blast_out_archive, blast_out_file),
                        shell=True)
    else:
        log.warn('blast: BLAST error!')
    return blast_out_archive


def psiblast_wrapper(fasta_in, cmd_extension, suffix):
    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)

    # psiblast run
    log.info("psiblast: running PSIBLAST... (using %i cores)" % cpu_count)
    psiblast_out_archive = os.path.join(basepath, filename + suffix + ".asn")
    psiblast_out_file = os.path.join(basepath, filename + suffix + ".blast")
    psiblast_out_xml = os.path.join(basepath, filename + suffix + ".xml")
    pssm_out = os.path.join(basepath, filename + suffix + ".pssm")
    cmd = '%s -db %s -query %s -outfmt 11 -out %s -out_ascii_pssm %s -num_threads %i %s' \
        % (PSIBLAST, BLAST_DB, fasta_in, psiblast_out_archive, pssm_out, cpu_count, cmd_extension)
    subprocess.call(cmd, shell=True)
    if os.path.isfile(psiblast_out_archive):
        subprocess.call('%s -archive %s -outfmt 5 -out %s' % (BLAST_FORMATTER, psiblast_out_archive, psiblast_out_xml),
                        shell=True)
        subprocess.call('%s -archive %s -outfmt 0 -out %s' % (BLAST_FORMATTER, psiblast_out_archive, psiblast_out_file),
                        shell=True)
    else:
        log.warn('psiblast: PSIBLAST error!')
    return psiblast_out_archive, pssm_out


def blast_extract_fasta(query_fasta, input, output, include_query=True, evalue_threshold=None):

    with open(input) as blast_out:
        blast_records = NCBIXML.parse(blast_out)

        ids = set()
        save_file = open(output, 'w')

        if include_query:
            query = read_query_sequence(query_fasta)
            save_file.write('>%s\n%s\n' % (query.id, query.seq))

        blast_records = list(blast_records)
        iterations = len(blast_records)
        log.info("psiblast: parsing iteration %i" % iterations)
        blast_record = blast_records[iterations - 1]

        for alignment in blast_record.alignments:
            skip_hsps = 0
            for hsp in alignment.hsps:
                skip_hsps += 1
                if skip_hsps > 1:
                    continue
                if alignment.title not in ids:
                    if evalue_threshold is not None and hsp.expect > evalue_threshold:
                        log.warn("psiblast: result filtered due to evalue threshold: %s > %s" % (hsp.expect, evalue_threshold))
                    else:
                        save_file.write('>%s\n%s\n' % (alignment.title, hsp.query))
                        ids.add(alignment.title)
                else:
                    log.warn("psiblast: duplicate sequence id: %s" % alignment.title)
        save_file.close()


def read_query_sequence(fasta_in, force_read=False):
    """Read query fasta file"""

    global query_sequence_obj
    if not query_sequence_obj or force_read:
        query_sequence_obj = SeqIO.read(fasta_in, "fasta")
        log.info("Read query sequence, id:%s length:%i" % (query_sequence_obj.id, len(query_sequence_obj.seq)))
    return query_sequence_obj


def msa_frequencies(fasta_in, format='fasta', all_letters='-XACDEFGHIKLMNPQRSTVWY', start=None, end=None, chars_to_ignore=''):
    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    msa = os.path.join(basepath, filename + ".mafft")

    log.info("msa_frequencies: calculating...")

    frequency_residue = {}
    frequency_overall = {}
    for residue in all_letters:
        frequency_overall[residue] = 0
    query = read_query_sequence(fasta_in)
    with open(msa, 'r') as msa_in:
        msao = AlignIO.read(msa_in, format)
        ali_length = msao.get_alignment_length()
        if not start:
            start = 0
        if not end:
            end = ali_length
        summary_info = AlignInfo.SummaryInfo(msao)
        query_seq_msa = (list(msao)[0]).seq
        residue_num = 0
        for msa_position in range(start, end):
            if query_seq_msa[msa_position] == "-":
                continue
            else:
                freq_dict = summary_info._get_letter_freqs(msa_position,
                                                   msao._records,
                                                   all_letters, chars_to_ignore)
                frequency_residue[residue_num] = freq_dict
                for residue in all_letters:
                    frequency_overall[residue] += freq_dict[residue]
                residue_num += 1
    for residue, freq in frequency_overall.items():
        frequency_overall[residue] = freq / residue_num

    query_frequencies = []
    for residue_num, residue in enumerate(query.seq):
        query_frequencies.append([residue, frequency_residue[residue_num][residue]])
    df_vector = pd.DataFrame(frequency_residue).T
    df_vector = df_vector.add_prefix('msaf_')
    df = pd.DataFrame(query_frequencies, columns=['WT_aa', 'msa_frequency'])
    df = pd.concat([df, df_vector], axis=1)
    df.index = np.arange(1, len(df.index) + 1)
    df.index.name = 'position'

    # FILTER FEATURES
    df = df.drop(["msaf_-", "msaf_A", "msaf_C", "msaf_D", "msaf_E", "msaf_F", "msaf_G", "msaf_H", "msaf_I", "msaf_K", "msaf_L", "msaf_M", "msaf_N", "msaf_P", "msaf_Q", "msaf_R", "msaf_S", "msaf_T", "msaf_V", "msaf_W", "msaf_X", "msaf_Y"], axis=1)

    return df, frequency_residue, frequency_overall


def aa_vectors(fasta_in):
    AA =  'ACDEFGHIKLMNPQRSTVWY'
    aa_vector = {}
    query = read_query_sequence(fasta_in)
    pos = 1
    for residue in query:
        vector = {}
        for aa in AA:
            if aa == residue:
                vector[aa] = 100
            else:
                vector[aa] = 0
        aa_vector[pos] = vector
        pos+=1
    df = pd.DataFrame(aa_vector).T
    df = df.add_prefix('aa_')
    return df


def label_by_cluster(dataset, query_id, min_scores=6):
    pandas2ri.activate()
    bblsuite_r = importr('bblsuite', robject_translations = {'protein.mutation.label_rheostat_cluster': 'protein_mutation_label_rheostat_cluster'})
    cluster_labels = bblsuite_r.protein_mutation_label_rheostat_cluster(dataset, datasets_wt[query_id], datasets_severe[query_id], min_scores)
    pandas2ri.deactivate()
    return pandas2ri.ri2py_dataframe(cluster_labels)


def csv2arff(fasta_in):
    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    csv_base = os.path.join(basepath, filename + ".features")

    log.info('converting csv to arff...')
    bash_command = '%s %s %s %s' % (CSVTOARFF, MODEL_R_TRAIN, csv_base, 'R')
    subprocess.call(bash_command, shell=True)

    bash_command = '%s %s %s %s' % (CSVTOARFF, MODEL_T_TRAIN, csv_base, 'T')
    subprocess.call(bash_command, shell=True)

    bash_command = '%s %s %s %s' % (CSVTOARFF, MODEL_N_TRAIN, csv_base, 'N')
    subprocess.call(bash_command, shell=True)


def csv2arff_combined(fasta_in):
    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    csv = os.path.join(basepath, filename + ".combined")

    log.info('converting csv to arff...')
    bash_command = '%s %s %s %s' % (CSVTOARFF, MODEL_4_TRAIN, csv, '4')
    subprocess.call(bash_command, shell=True)


def aucr(dataset, query_id):
    """Use R functions to calculate rheostat labels per residue."""

    dataset['rauc'] = np.nan
    for pos in range(1, len(dataset) + 1):
        scores = dataset.get_value(pos, 'scores')
        # log.debug(scores)
        if scores:
            rscores = robjects.FloatVector(scores)
            try:
                raucr = robjects.r('bblsuite::aucr(%s, auc_separator=%f)' % (rscores.r_repr(), datasets_wt[query_id]))[0]
            except:
                raucr = 0
            dataset.set_value(pos, 'rauc', raucr)
    # statistics = self.analyze_dataset(fasta_in, self.read_dataset(csv_in))
    # scores = statistics.get_value(0, 'scores')
    # log.debug(scores)
    # rscores = robjects.FloatVector(scores)
    # log.debug(rscores)
    # raucr = robjects.r('bblsuite::aucr(%s)' % rscores.r_repr())
    # log.debug(raucr)
    # importr('bblsuite')
    # rcmd_import_dataset = 'bblsuite::protein.mutation.import_dataset("%s")' % csv_in
    # rdataset = robjects.r(rcmd_import_dataset)
    # rcmd_label_rheostats = 'bblsuite::protein.mutation.label_rheostat(%s)' % rdataset.r_repr()
    # rlabels = robjects.r(rcmd_label_rheostats)
    # log.debug(rlabels)
    # return rlabels


def snap2aucr_old(fasta_in):
    basepath = os.path.dirname(fasta_in)
    snap2_csv = os.path.join(basepath, "snap2.csv")
    df = pd.DataFrame(
        columns=['position',
                 'WT_aa',
                 'snap2aucr']
    )

    log.info("snap2aucr: parsing & calculating...")

    with open(snap2_csv, 'r') as csv_in:
        snap2reader = csv.reader(csv_in, delimiter=',')
        #Variant,Predicted Effect,Score,Expected Accuracy
        header = next(snap2reader)
        currentPos = 0
        currentScores = []
        wt = None
        pos = None
        substitution = None
        for row in snap2reader:
            variant = re.match('([A-Z])([0-9]+)([A-Z])', row[0])
            wt = variant.group(1)
            pos = int(variant.group(2))
            substitution = variant.group(3)
            score = int(row[2])
            if currentPos == 0:
                currentPos = pos
            if currentPos != pos:
                rscores = robjects.FloatVector(currentScores)
                raucr = robjects.r('bblsuite::aucr(%s)' % rscores.r_repr())[0]
                df.set_value(currentPos, 'position', currentPos)
                df.set_value(currentPos, 'WT_aa', wt)
                df.set_value(currentPos, 'snap2aucr', raucr)
                currentPos = pos
                currentScores = []
            else:
                aa = Aminoacid(wt)
                #if (substitution in aa.get_possible_snps()):
                currentScores.append(score)
        rscores = robjects.FloatVector(currentScores)
        raucr = robjects.r('bblsuite::aucr(%s)' % rscores.r_repr())[0]
        df.set_value(currentPos, 'position', currentPos)
        df.set_value(currentPos, 'WT_aa', wt)
        df.set_value(currentPos, 'snap2aucr', raucr)
        df.index.name = 'position'
    return df


def snap2aucr_simple(fasta_in):
    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    snap2_csv = os.path.join(basepath, filename + ".snap2")
    df = pd.DataFrame(
        columns=['position',
                 'WT_aa',
                 'snap2aucr']
    )

    log.info("snap2aucr: parsing & calculating...")

    with open(snap2_csv, 'r') as csv_in:
        #snap2reader = csv.reader(csv_in, delimiter=',')
        #Variant,Predicted Effect,Score,Expected Accuracy
        header = next(csv_in)
        currentPos = 0
        currentScores = []
        wt = None
        pos = None
        substitution = None
        snap2_pattern = "([A-Z][0-9]+[A-Z])\s+(\S+)\s+([-]*[0-9]+)\s+([0-9]{1,3}%)"
        for row in csv_in:
            # variant = re.match('([A-Z])([0-9]+)([A-Z])', row[0])
            # wt = variant.group(1)
            # pos = int(variant.group(2))
            # substitution = variant.group(3)
            # score = int(row[2])
            row_ = re.findall(snap2_pattern, row)[0]
            variant = re.match('([A-Z])([0-9]+)([A-Z])', row_[0])
            wt = variant.group(1)
            pos = int(variant.group(2))
            substitution = variant.group(3)
            score = int(row_[2])
            if currentPos == 0:
                currentPos = pos
            if currentPos != pos:
                rscores = robjects.FloatVector(currentScores)
                raucr = robjects.r('bblsuite::aucr(%s)' % rscores.r_repr())[0]
                df.set_value(currentPos, 'position', currentPos)
                df.set_value(currentPos, 'WT_aa', wt)
                df.set_value(currentPos, 'snap2aucr', raucr)
                currentPos = pos
                currentScores = []
            else:
                aa = Aminoacid(wt)
                #if (substitution in aa.get_possible_snps()):
                currentScores.append(score)
        rscores = robjects.FloatVector(currentScores)
        raucr = robjects.r('bblsuite::aucr(%s)' % rscores.r_repr())[0]
        df.set_value(currentPos, 'position', currentPos)
        df.set_value(currentPos, 'WT_aa', wt)
        df.set_value(currentPos, 'snap2aucr', raucr)
        df.index.name = 'position'
    return df


def snap2aucr(fasta_in, query_id):
    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    snap2_csv = os.path.join(os.path.join(SNAP2_DB, filename), filename  + ".snap2")
    df = pd.DataFrame(
        columns=['position',
                 'WT_aa',
                 'snap2aucr']
    )

    log.info("snap2aucr: processing...")

    with open(snap2_csv, 'r') as csv_in:
        #snap2reader = csv.reader(csv_in, delimiter=',')
        #Variant,Predicted Effect,Score,Expected Accuracy
        #header = next(csv_in)
        currentPos = 0
        currentScores = []
        wt = None
        pos = None
        substitution = None
        snap2_pattern = "([A-Z][0-9]+[A-Z])\s+.*=\s+([-]*[0-9]+)"
        for row in csv_in:
            # variant = re.match('([A-Z])([0-9]+)([A-Z])', row[0])
            # wt = variant.group(1)
            # pos = int(variant.group(2))
            # substitution = variant.group(3)
            # score = int(row[2])
            row_ = re.findall(snap2_pattern, row)[0]
            variant = re.match('([A-Z])([0-9]+)([A-Z])', row_[0])
            wt = variant.group(1)
            pos = int(variant.group(2))
            substitution = variant.group(3)
            score = abs(int(row_[1]))
            if currentPos == 0:
                currentPos = pos
            if currentPos != pos:
                rscores = robjects.FloatVector(currentScores)
                raucr = robjects.r('bblsuite::aucr(%s, auc_separator=%i)' % (rscores.r_repr(), 10))[0]
                #raucr = robjects.r('bblsuite::aucr(%s)' % (rscores.r_repr()))[0]
                df.set_value(currentPos, 'position', currentPos)
                df.set_value(currentPos, 'WT_aa', wt)
                df.set_value(currentPos, 'snap2aucr', raucr)
                currentPos = pos
                currentScores = []
            else:
                #aa = Aminoacid(wt)
                #if (substitution in aa.get_possible_snps()):
                currentScores.append(score)
            next(csv_in)
        rscores = robjects.FloatVector(currentScores)
        raucr = robjects.r('bblsuite::aucr(%s)' % rscores.r_repr())[0]
        df.set_value(currentPos, 'position', currentPos)
        df.set_value(currentPos, 'WT_aa', wt)
        df.set_value(currentPos, 'snap2aucr', raucr)
        df.index = np.arange(1, len(df.index) + 1)
        df.index.name = 'position'
    return df


def possible_snps(fasta_in):
    query = read_query_sequence(fasta_in, force_read=True)

    df = pd.DataFrame(
        columns=['position',
                 'WT_aa',
                 'possible_snps']
    )

    for idx, residue in enumerate(query.seq):
        possibles = Aminoacid(residue).get_possible_snps()
        # df.set_value(idx, 'position', idx)
        df.at[idx, 'position'] = idx
        # df.set_value(idx, 'WT_aa', residue)
        df.at[idx, 'WT_aa'] = residue
        # df.set_value(idx, 'possible_snps', possibles)
        df.at[idx, 'possible_snps'] = possibles
    df.index = np.arange(1, len(df.index) + 1)
    df.index.name = 'position'

    return df


def xchange(fasta_in):
    query = read_query_sequence(fasta_in)

    df = pd.DataFrame(
        columns=['position',
                 'WT_aa',
                 'xchange']
    )

    for idx, residue in enumerate(query.seq):
        aa = Aminoacid(residue)
        possibles = aa.get_possible_snps()
        score = 0
        for aa_new in possibles:
            score += aa.get_basic_exchange_scoring(aa_new)
        df.set_value(idx, 'position', idx)
        df.set_value(idx, 'WT_aa', residue)
        df.set_value(idx, 'xchange', score)
    df.index = np.arange(1, len(df.index) + 1)
    df.index.name = 'position'

    return df


def aa_basic_prop(fasta_in):
    query = read_query_sequence(fasta_in)

    df = pd.DataFrame(columns=['position', 'WT_aa', 'hydrophobic', 'hydrophilic', 'aromatic', 'non-polar aliphatic', 'small', 'polar uncharged', 'negatively charged / acidic', 'positively charged / basic'])

    for idx, residue in enumerate(query.seq):
        aa = Aminoacid(residue)
        properties = aa.get_basic_properties()

        # df.set_value(idx, 'position', idx)
        df.at[idx, 'position'] = idx
        # df.set_value(idx, 'WT_aa', residue)
        df.at[idx, 'WT_aa'] = residue

        for c in df.columns[2:]:
            for p in properties:
                if c == p[1]:
                    # df.set_value(idx, c, 1)
                    df.at[idx, c] = 1
                    break
                else:
                    # df.set_value(idx, c, 0)
                    df.at[idx, c] = 0
    df.index = np.arange(1, len(df.index) + 1)
    df.index.name = 'position'

    return df


def single_models(fasta_in):
    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    test = os.path.join(basepath, filename)
    out = os.path.join(basepath, filename)

    log.info('Executing Model R...')
    bash_command = '%s %s %s.features_modelR.arff %s.prediction_modelR.csv %s' % (Msingle_RUN, MODEL_R_TRAIN, test, out, MODEL_R)
    subprocess.call(bash_command, shell=True)

    log.info('Executing Model T...')
    bash_command = '%s %s %s.features_modelT.arff %s.prediction_modelT.csv %s' % (Msingle_RUN, MODEL_T_TRAIN, test, out, MODEL_T)
    subprocess.call(bash_command, shell=True)

    log.info('Executing Model N...')
    bash_command = '%s %s %s.features_modelN.arff %s.prediction_modelN.csv %s' % (Msingle_RUN, MODEL_N_TRAIN, test, out, MODEL_N)
    subprocess.call(bash_command, shell=True)


def parse3state(fasta_in, suffix='.csv', weight_R=1, weight_T=1, weight_N=1):
    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    prefix = os.path.join(basepath, filename)

    predictions = ('T','R','N')

    all = {}

    for p in predictions:
        file = "%s.prediction_model%s%s" % (prefix, p, suffix)
        with open(file, 'r')as _in:
            csv_in = csv.reader(_in, delimiter=',')
            # inst,actual,predicted,error,prediction,id
            header = next(csv_in)
            for row in csv_in:
                if len(row) > 0:
                    actual = row[1][-1]
                    predicted = row[2][-1]
                    if row[3] == '+':
                        error = True
                    else:
                        error = False
                    prediction = float(row[4])

                    # if abs(prediction) < 0.9:
                    #     prediction = 0
                    if predicted == "O":
                        # negiated
                        # prediction = prediction * -1
                        prediction = (1 - prediction) - (1 - (1 - prediction))
                    else:
                        prediction = prediction - (1 - prediction)

                    if predicted == "O" and p in ['R']:
                       prediction = prediction * weight_R
                    elif predicted == "O" and p in ['T']:
                       prediction = prediction * weight_T
                    elif predicted == "O" and p in ['N']:
                       prediction = prediction * weight_N

                    # prediction = prediction * 100

                    id = int(row[5])
                    if id not in all:
                        all[id] = {'T':None,'R':None,'N':None}
                    all[id][p] = [predicted, error, prediction]
                    if suffix != '':
                        if actual != "O":
                            all[id]['actual'] = actual
                    elif actual != "O":
                        all[id]['actual'] = actual
                    all[id]['residue'] = row[6]

    out = []
    all_ids_sorted = sorted(all)
    out.append("id,residue,T,R,N,T_score,R_score,N_score,label\n")
    for i in all_ids_sorted:
        T = all[i]['T']
        R = all[i]['R']
        N = all[i]['N']
        if 'actual' not in all[i].keys():
            actual = "O"
        else:
            actual = all[i]['actual']
        residue = all[i]['residue']
        out.append("%i,%s,%s,%s,%s,%f,%f,%f,%s\n" % (i, residue, T[0], R[0], N[0], T[2], R[2], N[2], actual))
    with open('%s.combined%s' % (prefix, suffix), 'w') as _out:
        _out.writelines(out)


def model4(fasta_in):
    filename = os.path.splitext(os.path.basename(fasta_in))[0]
    basepath = os.path.dirname(fasta_in)
    test = os.path.join(basepath, filename)
    out = os.path.join(basepath, filename)

    log.info('Executing Model 4...')
    bash_command = '%s %s %s.combined_model4.arff %s.prediction_model4.csv %s' % (
        M4_RUN, MODEL_4_TRAIN, test, out, MODEL_4)
    subprocess.call(bash_command, shell=True)


def map_subsequence(fasta_in, full_in):
    query = read_query_sequence(fasta_in)
    full = read_query_sequence(full_in)

    return tools.is_subseq(query.seq, full.seq)
