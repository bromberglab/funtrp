#!/usr/bin/env python3

import sys
import traceback
import argparse
import os
import pandas as pd
if sys.version_info[0] < 3:
    raise Exception("mi-faser requires Python 3.x")
from weka.core import jvm
from bblsuite.helper import logger
from bblsuite.protein.rheostat import predictor

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'

log = logger.Logger.get_logger(console=False)
RHEOSTAT_BANDWITH_THRESHOLD_N = .35
RHEOSTAT_BANDWITH_THRESHOLD_T = .7
RHEOSTAT_FDR_THRESHOLD_N = .9
RHEOSTAT_FDR_THRESHOLD_T = .8


def get_argument_parser():
    """Argument parser if module is called from command line, see main method."""

    parser = argparse.ArgumentParser(description='Rheostat Predictor')
    parser.add_argument('query', metavar='query_id',
                        help='query sequence file in fasta format')
    parser.add_argument('-b', '--basedir', metavar='base_dir',
                        help='base directory containing datasets')
    parser.add_argument('-t', '--train', action='store_true',
                        help='training set or sets (id2,id1)')
    parser.add_argument('-p', '--predict', action='store_true',
                        help='predict')
    parser.add_argument('-e', '--evaluate', action='store_true',
                        help='evaluate model in train by LOO-CV')
    parser.add_argument('-m', '--model', default="RT",
                        help='select evaluation model [default:RT | SVM, LOG, RF]')
    parser.add_argument('-s', '--skip', action='store_true',
                        help='skip feature extraction before training/prediction')
    parser.add_argument('-x', '--extend', action='store_true',
                        help='extend features using experimental data')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='write out debug information and preliminary results')
    parser.add_argument('-o', '--output', metavar='output_dir',
                        help='base directory to save any prediction output')
    parser.add_argument('-l', '--modelout', metavar='model_dir',
                        help='base directory to save models')
    return parser


def main(args):
    if args.basedir:
        datadir = os.path.abspath(args.basedir)
    else:
        datadir = None
    query_id = args.query
    attributes_removed = [1, 2, 3]
    selected_features_manual = [2, 3, 4, 5, 6, 8, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27,
                                28, 29, 30, 31, 32, 39, 40, 41, 42, 49]
    model_dir = args.modelout
    output_dir = args.output

    if args.train:
        training_sets = query_id.split(',')

        trainings = []
        evaluate_single = args.evaluate

        if len(training_sets) > 1:
            evaluate_single = False
        for set_id in training_sets:
            basepath = os.path.join(datadir, set_id)
            training = predictor.Training(debug=args.debug)
            training.set_query(set_id, basepath, skip=args.skip, extend=args.extend,
                                          evaluate=evaluate_single, evlmodel=args.model)
            training.run()
            trainings.append(training)
        if len(trainings) > 1:
            log.info("Merging training sets...")
            fout_merged = os.path.join(datadir, os.path.join("merged", "merged.csv"))
            trainings_features = []
            for t in trainings:
                t.features['position'] = t.query_id + ":" + t.features['position'].astype(str)
                trainings_features.append(t.features)
            training_merged = pd.DataFrame(pd.concat(trainings_features))
            training_merged = training_merged.reset_index(drop=True)
            training_merged.to_csv(fout_merged, index=False, na_rep="NA")

            if args.evaluate:
                log.info('evaluating...')
                evaluation = predictor.PredEvaluation(type="CV-LOO", model_id=args.model)

                fout_base = os.path.join(datadir, os.path.join("merged", "merged_"))

                log.info("raw all sets: #total %i, #? %i, #U %i, #T %i, #R %i, #N %i" %
                         (
                             training_merged.shape[0],
                             training_merged[training_merged.label.isin(['?'])].shape[0],
                             training_merged[training_merged.label.isin(['U'])].shape[0],
                             training_merged[training_merged.label.isin(['T'])].shape[0],
                             training_merged[training_merged.label.isin(['R'])].shape[0],
                             training_merged[training_merged.label.isin(['N'])].shape[0]
                         )
                )

                training_filtered = training_merged[~training_merged.label.isin(['U', '?', 'R'])].reset_index(drop=True)
                # If feature list contains "score" (consCalc) this filter might be necessary to remove those entries with score == NA
                # training_filtered = training_filtered[~training_filtered.score.isnull()].reset_index(drop=True)

                log.info("filtered (N,T) all sets: #total %i, #? %i, #U %i, #T %i, #R %i, #N %i" %
                         (
                             training_filtered.shape[0],
                             training_filtered[training_filtered.label.isin(['?'])].shape[0],
                             training_filtered[training_filtered.label.isin(['U'])].shape[0],
                             training_filtered[training_filtered.label.isin(['T'])].shape[0],
                             training_filtered[training_filtered.label.isin(['R'])].shape[0],
                             training_filtered[training_filtered.label.isin(['N'])].shape[0]
                         )
                )

                # get list of manually selected to remove from training set
                manually_selected = fout_base + "manually_selected.csv"
                with open(manually_selected) as csv_in:
                    df_manually_selected = pd.read_csv(csv_in)

                # T vs N all
                # fout_TvsN = fout_base + "TvsN.csv"
                # fout_TvsN_p = fout_base + "TvsN_predictions.csv"
                # TvsN = training_filtered.copy()
                # TvsN.to_csv(fout_TvsN, index=False, na_rep="NA")
                # evaluation.run_single(fout_TvsN, fout_TvsN_p, remove=attributes_removed)

                # T vs N refined
                fout_TvsN_refined = fout_base + "TvsN_refined.csv"
                fout_TvsN_refined_p = fout_base + "TvsN_refined_predictions.csv"
                TvsN_refined = training_filtered.copy()
                TvsN_refined = TvsN_refined[~TvsN_refined.position.isin(list(df_manually_selected.position))].reset_index(drop=True)
                TvsN_refined.to_csv(fout_TvsN_refined, index=False, na_rep="NA")

                # # # Feature Selection
                # automated
                # fout_TvsN_refined_features = fout_base + "TvsN_refined_features.csv"
                # fout_TvsN_refined_features_info = fout_base + "TvsN_refined_features_info.csv"
                # fs = predictor.FeatureSelection(dataset=None)
                # selected_features, selected_feature_names, selected_feature_avg_scores = fs.evaluate(dataset_csv=fout_TvsN_refined, start=0, end=TvsN_refined.shape[0], outfile=fout_TvsN_refined_features, remove=attributes_removed)
                # total_feature_count = TvsN_refined.shape[1]
                # filtered_feature_count = total_feature_count - len(attributes_removed) - 1
                # selected_features_reindexd = set(range(1, (filtered_feature_count + 1)))
                # for findex in selected_features:
                #     selected_features_reindexd.remove(findex + 1)
                # with open(fout_TvsN_refined_features_info, 'w') as fout:
                #     fout.write("--- Automated feature selection ---" + '\n')
                #     fout.write("#features total=%i removed=%i selected=%i/%i" %
                #              (total_feature_count, len(attributes_removed), len(selected_features),
                #               filtered_feature_count) + '\n')
                #     fout.write(str(selected_features) + '\n')
                #     fout.write(str(selected_feature_names) + '\n')
                #     fout.write(str(selected_feature_avg_scores) + '\n')

                # manual
                # total_feature_count = TvsN_refined.shape[1]
                # filtered_feature_count = total_feature_count - len(attributes_removed) - 1
                # selected_features_reindexd_manual = set(range(1, (filtered_feature_count + 1)))
                # for findex in selected_features_manual:
                #     selected_features_reindexd_manual.remove(findex)
                # log.info("#manually selected features total=%i removed=%i selected=%i/%i" %
                #          (total_feature_count, len(attributes_removed), len(selected_features_manual), filtered_feature_count))

                evaluation.run_single(fout_TvsN_refined, fout_TvsN_refined_p, remove=attributes_removed, limit_features=None)

                # Train T vs N
                training_tvsn = predictor.Training(debug=args.debug)
                TvsN_model = training_tvsn.train_model(fout_TvsN_refined, args.model, remove=attributes_removed,
                                                  limit_features=None, saveas=os.path.join(model_dir, "TvsN.model"))
                prediction = predictor.Prediction(TvsN_model)

                # repredict training set
                fout_TvsN_predict_training_p = fout_base + "TvsN_predictions_training.csv"
                prediction.predict_model(fout_TvsN_refined, addcolumns=[1, 3], saveAs=fout_TvsN_predict_training_p)

                # Use T vs N model for predictions on remaining [U, R] labeled instances
                fout_TvsN_predict_R = fout_base + "TvsN_R_all.csv"
                fout_TvsN_predict_R_p = fout_base + "TvsN_predictions_R_all.csv"
                TvsN_predict_R = training_merged.copy()
                # TvsN_predict_R = TvsN_predict_R[TvsN_predict_R.label.isin(['?', 'U', 'R'])].reset_index(drop=True)
                # TvsN_predict_R.label = 'U'
                TvsN_predict_R = TvsN_predict_R[TvsN_predict_R.label.isin(['U', 'R', '?'])].reset_index(drop=True)
                TvsN_predict_R.label = 'U'
                TvsN_predict_R.to_csv(fout_TvsN_predict_R, index=False, na_rep="NA")
                TvsN_R_predictions, TvsN_R_csv = prediction.predict_model(fout_TvsN_predict_R, addcolumns=[1, 3],
                                                                      saveAs=fout_TvsN_predict_R_p)

                # Define new [R]heostat labels through cutoffs
                new_label_cnt = 0
                new_label_idx = []
                new_labels_idx = {'T':[], 'N':[]}
                for preds in TvsN_R_predictions:
                    predlabelidx = preds[1]
                    labels = preds[2]
                    scores = preds[3]
                    label = labels[predlabelidx]
                    set_idx = preds[-1].split(',')[-2]
                    new_label = ''
                    difference = (scores[1] - scores[0])
                    # if abs(difference) < RHEOSTAT_BANDWITH_THRESHOLD:
                    idxT = labels.index('T')
                    idxN = labels.index('N')
                    if predlabelidx == idxN:
                        combined_score = 1 - scores[idxN]
                    else:
                        combined_score = scores[idxT]
                    # note: combined score is TOGGLE prediction;
                    # 0 is strongest NEUTRAL prediction - 1 is strongest TOGGLE prediction
                    if RHEOSTAT_BANDWITH_THRESHOLD_N <= combined_score <= RHEOSTAT_BANDWITH_THRESHOLD_T:
                        new_label = 'R'
                        new_label_cnt += 1
                        new_label_idx.append(set_idx)
                        if label == "N":
                            score = scores[idxN]
                        elif label == "T":
                            score = scores[idxT]
                    elif label == 'T':
                        score = scores[idxT]
                        if score >= RHEOSTAT_FDR_THRESHOLD_T:
                            new_labels_idx[label].append(set_idx)
                    elif label == 'N':
                        score = scores[idxN]
                        if score >= RHEOSTAT_FDR_THRESHOLD_N:
                            new_labels_idx[label].append(set_idx)

                    # score = scores[predlabelidx]
                    # print(scores, score, label, difference, new_label, set_idx)
                log.info("New Rheostat labels: %i | toggles: %i, neutrals: %i" % (new_label_cnt, len(new_labels_idx['T']), len(new_labels_idx['N'])))

                # re-label eberything to [U], then add new R labels and save set
                fout_new_labeled = fout_base + "new_rheostat_labels_plus_TN.csv"
                new_labeled = training_merged.copy()
                new_labeled = new_labeled[~new_labeled.position.isin(list(df_manually_selected.position))].reset_index(drop=True)
                for index, row in new_labeled.iterrows():
                    idx = row['position']
                    old_label = new_labeled.at[index, "label"]
                    if old_label == "R":
                        new_labeled.ix[index, "label"] = "U"
                    if idx in new_label_idx:
                        new_labeled.ix[index, "label"] = "R"
                        if old_label == "T":
                            log.warn("Re-labeling toggle!")
                        elif old_label == "N":
                            log.warn("Re-labeling neutral!")
                    elif idx in new_labels_idx['T']:
                        new_labeled.ix[index, "label"] = "T"
                        #print(new_labeled.ix[index, "position"] + ",T")
                        if old_label == "N":
                            log.warn("Re-labeling neutral as toggle!")
                    elif idx in new_labels_idx['N']:
                        new_labeled.ix[index, "label"] = "N"
                        #print(new_labeled.ix[index, "position"] + ",N")
                        if old_label == "T":
                            log.warn("Re-labeling toggle as neutral!")
                fout_new_labeled_p = fout_base + "new_rheostat_labels_predictions_plus_TN.csv"
                new_labeled_U_only = new_labeled[new_labeled.label.isin(['U'])].reset_index(drop=True)
                new_labeled = new_labeled[~new_labeled.label.isin(['U', '?'])].reset_index(drop=True)
                # If feature list contains "score" (consCalc) this filter might be necessary to remove those entries with score == NA
                # new_labeled_U_only = new_labeled_U_only[~new_labeled_U_only.score.isnull()].reset_index(drop=True)
                # new_labeled = new_labeled[~new_labeled.score.isnull()].reset_index(drop=True)
                new_labeled.to_csv(fout_new_labeled, index=False, na_rep="NA")

                log.info("re-labeled: #total %i, #? %i, #U %i, #T %i, #R %i, #N %i" %
                         (
                             new_labeled.shape[0],
                             new_labeled[new_labeled.label.isin(['?'])].shape[0],
                             new_labeled[new_labeled.label.isin(['U'])].shape[0],
                             new_labeled[new_labeled.label.isin(['T'])].shape[0],
                             new_labeled[new_labeled.label.isin(['R'])].shape[0],
                             new_labeled[new_labeled.label.isin(['N'])].shape[0]
                         )
                )

                # get list of manually selected to remove from training set
                manually_selected_newly_labeled_R = fout_base + "manually_selected_newly_labeled_R.csv"
                with open(manually_selected_newly_labeled_R) as csv_in:
                    df_manually_selected_newly_labeled_R = pd.read_csv(csv_in)

                # T vs N vs R refined
                fout_TvsNvsR_refined_2 = fout_base + "TvsNvsR_refined_2.csv"
                fout_TvsN_refined_2_p = fout_base + "TvsNvsR_refined_2_predictions.csv"
                TvsNvsR_refined_2 = new_labeled.copy()
                TvsNvsR_refined_2 = TvsNvsR_refined_2[
                    ~TvsNvsR_refined_2.position.isin(list(df_manually_selected_newly_labeled_R.position))].reset_index(drop=True)
                TvsNvsR_refined_2.to_csv(fout_TvsNvsR_refined_2, index=False, na_rep="NA")

                log.info("re-labeled refined: #total %i, #? %i, #U %i, #T %i, #R %i, #N %i" %
                         (
                             TvsNvsR_refined_2.shape[0],
                             TvsNvsR_refined_2[TvsNvsR_refined_2.label.isin(['?'])].shape[0],
                             TvsNvsR_refined_2[TvsNvsR_refined_2.label.isin(['U'])].shape[0],
                             TvsNvsR_refined_2[TvsNvsR_refined_2.label.isin(['T'])].shape[0],
                             TvsNvsR_refined_2[TvsNvsR_refined_2.label.isin(['R'])].shape[0],
                             TvsNvsR_refined_2[TvsNvsR_refined_2.label.isin(['N'])].shape[0]
                         )
                )

                # # # Feature Selection
                # automated
                # fout_new_labeled_features = fout_base + "new_rheostat_labels_features.csv"
                # fout_new_labeled_features_info = fout_base + "new_rheostat_labels_features_info.csv"
                # fs = predictor.FeatureSelection(dataset=None)
                # selected_features, selected_feature_names, selected_feature_avg_scores = fs.evaluate(dataset_csv=fout_new_labeled, start=0, end=new_labeled.shape[0],
                #                                 outfile=fout_new_labeled_features, remove=attributes_removed)
                # total_feature_count = new_labeled.shape[1]
                # filtered_feature_count = total_feature_count - len(attributes_removed) - 1
                # selected_features_reindexd = set(range(1, (filtered_feature_count + 1)))
                # for findex in selected_features:
                #     selected_features_reindexd.remove(findex + 1)
                # with open(fout_new_labeled_features_info, 'w') as fout:
                #     fout.write("--- Automated feature selection ---" + '\n')
                #     fout.write("#features total=%i removed=%i selected=%i/%i" %
                #              (total_feature_count, len(attributes_removed), len(selected_features),
                #               filtered_feature_count) + '\n')
                #     fout.write(str(selected_features) + '\n')
                #     fout.write(str(selected_feature_names) + '\n')
                #     fout.write(str(selected_feature_avg_scores) + '\n')

                # log.info("#manually selected features total=%i removed=%i selected=%i/%i" %
                #          (total_feature_count, len(attributes_removed), len(selected_features_manual),
                #           filtered_feature_count))

                # T vs N vs R refined
                evaluation.run_single(fout_TvsNvsR_refined_2, fout_TvsN_refined_2_p, remove=attributes_removed, limit_features=None)

                # Train T vs N vs R
                training_tvsnvsr = predictor.Training(debug=args.debug)
                TvsNvsR_model = training_tvsnvsr.train_model(fout_TvsNvsR_refined_2, args.model, remove=attributes_removed,
                                                  limit_features=None, saveas=os.path.join(model_dir, "TvsNvsR.model"))
                prediction = predictor.Prediction(TvsNvsR_model)

                # repredict training set
                fout_TvsNvsR_predict_training_p = fout_base + "TvsNvsR_refined_2_predictions_training.csv"
                prediction.predict_model(fout_TvsNvsR_refined_2, addcolumns=[1, 3], saveAs=fout_TvsNvsR_predict_training_p)

                # Use T vs N vs R model for predictions on remaining [U] labeled instances
                fout_TvsNvsR_predict = fout_base + "TvsNvsR.csv"
                fout_TvsNvsR_predict_p = fout_base + "TvsNvsR_predictions.csv"
                new_labeled_U_only.to_csv(fout_TvsNvsR_predict, index=False, na_rep="NA")
                TvsNvsR_predictions, TvsNvsR_csv = prediction.predict_model(fout_TvsNvsR_predict, addcolumns=[1, 3],
                                                                            saveAs=fout_TvsNvsR_predict_p)

                ########################################################################################################
                # Consurf Split Start >>>>
                # training_filtered_withtoggle_split = new_labeled[new_labeled.consurf_score <= -.097]
                # training_filtered_notoggles_split = new_labeled[new_labeled.consurf_score > -.097]
                # log.info("split (consurf): #with_toggles %i, #no_toggles %i" %
                #          (
                #              training_filtered_withtoggle_split.shape[0],
                #              training_filtered_notoggles_split.shape[0]
                #          )
                # )
                # log.info("with-toggle: #total %i, #? %i, #U %i, #T %i, #R %i, #N %i" %
                #          (
                #              training_filtered_withtoggle_split.shape[0],
                #              training_filtered_withtoggle_split[training_filtered_withtoggle_split.label.isin(['?'])].shape[0],
                #              training_filtered_withtoggle_split[training_filtered_withtoggle_split.label.isin(['U'])].shape[0],
                #              training_filtered_withtoggle_split[training_filtered_withtoggle_split.label.isin(['T'])].shape[0],
                #              training_filtered_withtoggle_split[training_filtered_withtoggle_split.label.isin(['R'])].shape[0],
                #              training_filtered_withtoggle_split[training_filtered_withtoggle_split.label.isin(['N'])].shape[0]
                #          )
                # )
                # log.info("no-toggle: #total %i, #? %i, #U %i, #T %i, #R %i, #N %i" %
                #          (
                #              training_filtered_notoggles_split.shape[0],
                #              training_filtered_notoggles_split[training_filtered_notoggles_split.label.isin(['?'])].shape[0],
                #              training_filtered_notoggles_split[training_filtered_notoggles_split.label.isin(['U'])].shape[0],
                #              training_filtered_notoggles_split[training_filtered_notoggles_split.label.isin(['T'])].shape[0],
                #              training_filtered_notoggles_split[training_filtered_notoggles_split.label.isin(['R'])].shape[0],
                #              training_filtered_notoggles_split[training_filtered_notoggles_split.label.isin(['N'])].shape[0]
                #          )
                # )

                # fout_model1 = fout_base + "model1.csv"
                # fout_model1_p = fout_base + "model1_p.csv"
                # training_filtered_withtoggle_split.to_csv(fout_model1, index=False, na_rep="NA")
                # evaluation.run_single(fout_model1, fout_model1_p, remove=attributes_removed)#, 9, 10, 11, 12, 18])
                #
                # fout_model2 = fout_base + "model2.csv"
                # fout_model2_p = fout_base + "model2_p.csv"
                # training_filtered_notoggles_split.to_csv(fout_model2, index=False, na_rep="NA")
                # evaluation.run_single(fout_model2, fout_model2_p, remove=attributes_removed)#, 9, 10, 11, 12, 18])

                # Consurf Split End   <<<<
                ########################################################################################################

                ########################################################################################################
                # DEPRECATED
                # # T vs N vs R refined
                # evaluation = predictor.PredEvaluation(type="CV-LOO", model_id=args.model)
                # fout_TvsNvsR_refined = fout_base + "TvsNvsR_refined.csv"
                # fout_TvsNvsR_refined_p = fout_base + "TvsNvsR_refined_predictions.csv"
                # TvsNvsR_refined = training_merged.copy()
                # TvsNvsR_refined = TvsNvsR_refined[~TvsNvsR_refined.label.isin(['?'])].reset_index(drop=True)
                # TvsNvsR_refined = TvsNvsR_refined[~TvsNvsR_refined.score.isnull()].reset_index(drop=True)
                # TvsNvsR_refined = TvsNvsR_refined[
                #     ~TvsNvsR_refined.position.isin(list(df_manually_selected.position))
                # ].reset_index(drop=True)
                # TvsNvsR_refined.label = TvsNvsR_refined.label.replace(['R'], 'U')
                # TvsNvsR_refined.to_csv(fout_TvsNvsR_refined, index=False, na_rep="NA")
                # evl, positions, data = evaluation.run_single(fout_TvsNvsR_refined, fout_TvsNvsR_refined_p, remove=attributes_removed, )
                #
                # class_labels = evl.header.attribute_by_name('label').values
                # R_positions = list()
                # filtered_positions = list()
                # for i in range(0, len(evl.predictions)):
                #     pred = evl.predictions[i]
                #     pos = positions[i]
                #     actual = class_labels[int(pred.actual)]
                #     predicted = class_labels[int(pred.predicted)]
                #     score = pred.distribution[int(pred.predicted)]
                #     if (predicted == "U"):
                #         if (actual == "U"):
                #             if (score >= .65):
                #                 R_positions.append(pos)
                #             else:
                #                 filtered_positions.append(pos)
                #         elif (actual == "T" or actual == "N"):
                #             if (score >= .2):
                #                 filtered_positions.append(pos)
                #
                # fout_TvsNvsR_refined_2 = fout_base + "TvsNvsR_refined_2.csv"
                # fout_TvsNvsR_refined_2_p = fout_base + "TvsNvsR_refined_2_predictions.csv"
                # TvsNvsR_refined_2 = training_merged.copy()
                # TvsNvsR_refined_2 = TvsNvsR_refined_2[~TvsNvsR_refined_2.label.isin(['?'])].reset_index(drop=True)
                # TvsNvsR_refined_2 = TvsNvsR_refined_2[~TvsNvsR_refined_2.score.isnull()].reset_index(drop=True)
                # TvsNvsR_refined_2 = TvsNvsR_refined_2[
                #     ~TvsNvsR_refined_2.position.isin(list(df_manually_selected.position))
                # ].reset_index(drop=True)
                # TvsNvsR_refined_2 = TvsNvsR_refined_2[
                #     ~TvsNvsR_refined_2.position.isin(filtered_positions)
                # ].reset_index(drop=True)
                # TvsNvsR_refined_2.label = TvsNvsR_refined_2.label.replace(['U'], 'R')
                # TvsNvsR_refined_2.to_csv(fout_TvsNvsR_refined_2, index=False, na_rep="NA")
                #
                # # Train T vs N vs R
                # evaluation.run_single(fout_TvsNvsR_refined_2, fout_TvsNvsR_refined_2_p, remove=attributes_removed)
                ########################################################################################################

                ########################################################################################################
                # DEPRECATED
                # # RvsO
                # fout_RvsO = fout_base + "RvsO.csv"
                # fout_RvsO_p = fout_base + "RvsO_predictions.csv"
                # RvsO = training_merged.copy()
                # RvsO.ix[RvsO.label.isin(['T','N']), 'label'] = 'O'
                # RvsO.to_csv(fout_RvsO, index=False, na_rep="NA")
                # # evaluation.run_single(fout_RvsO, fout_RvsO_p)
                #
                # # TvsO
                # fout_TvsO = fout_base + "TvsO.csv"
                # fout_TvsO_p = fout_base + "TvsO_predictions.csv"
                # TvsO = training_merged.copy()
                # TvsO.ix[TvsO.label.isin(['R', 'N']), 'label'] = 'O'
                # TvsO.to_csv(fout_TvsO, index=False, na_rep="NA")
                # # evaluation.run_single(fout_TvsO, fout_TvsO_p)
                #
                # # NvsO
                # fout_NvsO = fout_base + "NvsO.csv"
                # fout_NvsO_p = fout_base + "NvsO_predictions.csv"
                # NvsO = training_merged.copy()
                # NvsO.ix[NvsO.label.isin(['R', 'T']), 'label'] = 'O'
                # NvsO.to_csv(fout_NvsO, index=False, na_rep="NA")
                # # evaluation.run_single(fout_NvsO, fout_NvsO_p)
                #
                # # R vs T
                # fout_RvsT = fout_base + "RvsT.csv"
                # fout_RvsT_p = fout_base + "RvsT_predictions.csv"
                # RvsT = training_merged.copy()
                # RvsT = RvsT[~RvsT.label.isin(['N'])]
                # RvsT.to_csv(fout_RvsT, index=False, na_rep="NA")
                # # evaluation.run_single(fout_RvsT, fout_RvsT_p)
                #
                # # R vs N
                # fout_RvsN = fout_base + "RvsN.csv"
                # fout_RvsN_p = fout_base + "RvsN_predictions.csv"
                # RvsN = training_merged.copy()
                # RvsN = RvsN[~RvsN.label.isin(['T'])]
                # RvsN.to_csv(fout_RvsN, index=False, na_rep="NA")
                # # evaluation.run_single(fout_RvsN, fout_RvsN_p)
                #
                # # T vs N
                # fout_TvsN = fout_base + "TvsN.csv"
                # fout_TvsN_p = fout_base + "TvsN_predictions.csv"
                # TvsN = training_merged.copy()
                # TvsN = TvsN[~TvsN.label.isin(['R'])]
                # TvsN.to_csv(fout_TvsN, index=False, na_rep="NA")
                # # evaluation.run_single(fout_TvsN, fout_TvsN_p)
                #
                # # evaluation.parse3state(fout_merged)
                #
                # fout_RvsT = fout_base + "RvsT.csv"
                # fout_RvsT_p = fout_base + "RvsT_predictions.csv"
                # fout_RvsT_all_p = fout_base + "RvsT_predictions_all.csv"
                # training = predictor.Training()
                # trained_model = training.train_model(fout_RvsT, args.model, remove=[1, 3])
                # prediction = predictor.Prediction(trained_model)
                # RvsT_predictions, RvsT_csv = prediction.predict_model(fout_evaluation, addcolumns=[1, 3], saveAs=fout_RvsT_all_p)
                #
                #
                # # predictions
                # R_O = evaluation.parse_eval(fout_RvsO_p)
                # T_O = evaluation.parse_eval(fout_TvsO_p)
                # N_O = evaluation.parse_eval(fout_NvsO_p)
                # R_T_N = evaluation.parse_eval(fout_evaluation_p)
                #
                # ids = []
                # import csv
                # with open(fout_evaluation, 'r') as csv_in:
                #     csv_reader = csv.reader(csv_in, delimiter=',')
                #     header = next(csv_reader)
                #     for row in csv_reader:
                #         ids.append((row[0], row[2], row[-1]))
                #
                # predictions = []
                # predictions.append("id,res,pR,pT,pN,pRT,sR,sT,sN,sRT,label\n")
                # # (instance, actual, predicted, error, prediction, id, residue)
                # for i in ids:
                #     id = i[0]
                #     residue = i[1]
                #     label = i[2]
                #     predicted_R = R_O[id][2]
                #     prediction_R = R_O[id][4]
                #     predicted_T = T_O[id][2]
                #     prediction_T = T_O[id][4]
                #     predicted_N = N_O[id][2]
                #     prediction_N = N_O[id][4]
                #     predicted_RTN = R_T_N[id][2]
                #     prediction_RTN = R_T_N[id][4]
                #     predictions.append("%s,%s,%s,%s,%s,%s,%f,%f,%f,%f,%s\n" % (id, residue, predicted_R, predicted_T, predicted_N, predicted_RTN, prediction_R, prediction_T, prediction_N, prediction_RTN, label))
                # fout_RvsT_p = fout_base + "3stat_predictions.csv"
                # with open(fout_RvsT_p, 'w') as fout:
                #     fout.writelines(predictions)
                ########################################################################################################


    if args.predict:
        prediction = predictor.Prediction(model=None, debug=args.debug)
        prediction.set_query(query_id, basepath=args.basedir, skip=args.skip)
        prediction.run()

        model1 = predictor.Model()
        model1.load_model(os.path.join(model_dir, "TvsN.model"))

        prediction.model = model1
        TvsN_predictions, TvsN_csv = prediction.predict_model(prediction.fout_features, addcolumns=[1, 3],
                                    saveAs=os.path.join(output_dir, prediction.query_id + ".TvsN.predictions.csv"))

        model2 = predictor.Model()
        model2.load_model(os.path.join(model_dir, "TvsNvsR.model"))

        prediction.model = model2
        TvsNvsR_predictions, TvsNvsR_csv = prediction.predict_model(prediction.fout_features, addcolumns=[1, 3],
                                    saveAs=os.path.join(output_dir, prediction.query_id + ".TvsNvsR.predictions.csv"))


if __name__ == "__main__":

    try:
        jvm.start()
        main(get_argument_parser().parse_args())
    except Exception as e:
        print(traceback.format_exc())
    finally:
        jvm.stop()
