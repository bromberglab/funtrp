#!/usr/bin/env python3
import csv
import argparse
import os
import re
import pandas as pd
import numpy as np
from Bio import SeqIO
from bblsuite.protein.aminoacid import aminoacid_main
from bblsuite.helper import logger

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'

log = logger.Logger.get_logger(console=True)

def get_column_names():
    """Static function to get column names defined when exporting parsed datasets as csv/tsv."""

    return ['position', 'wt'] + aminoacid_main.Aminoacid.get_all_one_letter()


class DMSDatasetParser:

    log = logger.Logger.get_logger()

    def __init__(self):
        self.parser_index = {}
        self.init_parser_index()

    def init_parser_index(self):
        """Add any new parser function here to be registered."""

        self.add_parser_function('1', parser_1, "Parser for dataset_1")
        self.add_parser_function('2', parser_2, "Parser for dataset_2")
        self.add_parser_function('3', parser_3, "Parser for dataset_3")
        self.add_parser_function('7', parser_7, "Parser for dataset_7")
        self.add_parser_function('8', parser_8, "Parser for dataset_8")
        self.add_parser_function('11', parser_11, "Parser for dataset_11")
        self.add_parser_function('12', parser_12, "Parser for dataset_12")
        self.add_parser_function('13', parser_13, "Parser for dataset_13")
        self.add_parser_function('15', parser_15, "Parser for dataset_15")
        self.add_parser_function('17', parser_17, "Parser for dataset_17")
        self.add_parser_function('22', parser_22, "Parser for dataset_22")
        self.add_parser_function('24', parser_24, "Parser for dataset_24")
        self.add_parser_function('25', parser_25, "Parser for dataset_25")
        self.add_parser_function('27', parser_27, "Parser for dataset_27")
        self.add_parser_function('42', parser_42, "Parser for dataset_42")
        self.add_parser_function('rdist', parser_dist_analysis, "Parser for R distribution analysis")
        self.add_parser_function('som', som_rheostat_parser, "Parser for som_rheostat_paper")
        self.add_parser_function('prof', profparser, "parser for prof results")

    def add_parser_function(self, id, func, description):
        """Adds a new parser function to the internal index"""

        self.parser_index[id] = [func, description]

    def get_parser_function(self, parser_id):
        """Returns a parser function from the internal index"""

        log.info(self.parser_index.get(parser_id)[1])
        return self.parser_index.get(parser_id)[0]

    def get_parser_description(self, parser_id):
        """Returns a parser function description from the internal index"""

        return self.parser_index.get(parser_id)[1]

    def parse_dataset(self, parser_id, *params):
        """Parses a given dataset using a given parser function (referred by id)."""

        self.log.info('parsing dataset...')
        parser_function = self.get_parser_function(parser_id)
        return parser_function(*params)

    @staticmethod
    def read_dataset(csv_in):
        """Read dataset which has been parsed and exported earlier."""

        return pd.read_csv(csv_in)


def parser_1(csv_in, fasta_in, *args):

    dataset = pd.DataFrame(columns=get_column_names())
    offset = -1
    seq = ''
    current_positions = []
    all_variants = 0
    valid_variants = 0
    na_variants = 0
    filtered_variants = 0
    with open(csv_in) as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter=',')

        idx = 0
        for row in tsv_in:
            if idx < 2:
                idx += 1
                continue

            regepx_match = re.match(r'([A-Z])([0-9]+)([A-Z,*])', row[0], re.M | re.I)
            wt = regepx_match.group(1)
            position = int(regepx_match.group(2))
            substitution = regepx_match.group(3)

            score = row[4]
            filter = row[10]

            if score == 'NA':
                na_variants += 1
            else:
                score = float(score)
                valid_variants += 1

            if filter != 'Pass':
                filtered_variants += 1
                valid_variants -= 1
                score = 'NA'

            if position not in current_positions:
                current_positions += [position]
                seq += wt

            all_variants += 1

            dataset.set_value(position, 'position', position + offset)
            dataset.set_value(position, 'wt', wt)
            dataset.set_value(position, substitution, score)

    seq = list(seq)

    # this zips seq and current_positions and sorts seq
    sorted_y_idx_list = sorted(range(len(current_positions)), key=lambda x: current_positions[x])
    sorted_seq = [seq[i] for i in sorted_y_idx_list]

    with open(fasta_in, 'w') as output_fasta_file:
        output_fasta_file.write('>dataset_1\n')
        output_fasta_file.write(''.join(sorted_seq))

    log.info("All variants: %i" % all_variants)
    log.info("NA variants: %i" % na_variants)
    log.info("Filtered variants: %i" % filtered_variants)
    log.info("Valid variants: %i (%f)" % (valid_variants, valid_variants/all_variants))
    return dataset


def parser_2(csv_in, fasta_in, *args):

    # dataset = pd.DataFrame(columns=get_column_names())
    # seq = ''
    # current_positions = []
    # all_variants = 0
    # valid_variants = 0
    # na_variants = 0
    # filtered_variants = 0
    # with open(csv_in) as tsv_in:
    #     tsv_in = csv.reader(tsv_in, delimiter=',')
    #
    #     idx = 0
    #     for row in tsv_in:
    #         if idx < 6:
    #             idx += 1
    #             continue
    #
    #         regepx_match = re.match(r'([A-Z])([0-9]+)([A-Z,*])', row[0], re.M | re.I)
    #         wt = regepx_match.group(1)
    #         position = int(regepx_match.group(2))
    #         substitution = regepx_match.group(3)
    #         score = row[2]
    #
    #         if not score:
    #             score = 'NA'
    #             na_variants += 1
    #         else:
    #             score = float(score)
    #             valid_variants += 1
    #
    #         if position not in current_positions:
    #             current_positions += [position]
    #             seq += wt
    #
    #         all_variants += 1
    #
    #         dataset.set_value(position, 'position', position)
    #         dataset.set_value(position, 'wt', wt)
    #         dataset.set_value(position, substitution, score)
    #
    # seq = list(seq)
    #
    # # this zips seq and current_positions and sorts seq
    # sorted_y_idx_list = sorted(range(len(current_positions)), key=lambda x: current_positions[x])
    # sorted_seq = [seq[i] for i in sorted_y_idx_list]
    #
    # with open(fasta_in, 'w') as output_fasta_file:
    #     output_fasta_file.write('>dataset_1\n')
    #     output_fasta_file.write(''.join(sorted_seq))
    #
    # log.info("All variants: %i" % all_variants)
    # log.info("NA variants: %i" % na_variants)
    # log.info("Filtered variants: %i" % filtered_variants)
    # log.info("Valid variants: %i (%f)" % (valid_variants, valid_variants / all_variants))
    # return dataset
    log.error("NOT IMPLEMENTED")


def parser_3(csv_in, fasta_in, *args):
    """
    Parser for dataset 3.

    Here the original dataset is already in the correct format.
    The only changes are the relabeling of the columns,
    whereas the order is already correct.
    """

    seq = ''
    current_positions = []
    all_variants = 0
    valid_variants = 0
    na_variants = 0
    filtered_variants = 0

    # read the dataset into a DataFrame structure
    dataset = pd.read_csv(csv_in)
    # replace column names
    dataset.columns = get_column_names()
    # renumber
    dataset['position'] = np.arange(1, len(dataset) + 1)

    with open(csv_in) as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter=',')

        # skip header
        next(tsv_in)

        for row in tsv_in:
            wt = row[1]
            position = row[0]

            for score in row[2:]:
                if not score:
                    na_variants += 1
                else:
                    valid_variants += 1

                all_variants += 1

            if position not in current_positions:
                current_positions += [position]
                seq += wt

    seq = list(seq)

    # this zips seq and current_positions and sorts seq
    sorted_y_idx_list = sorted(range(len(current_positions)), key=lambda x: current_positions[x])
    sorted_seq = [seq[i] for i in sorted_y_idx_list]

    with open(fasta_in, 'w') as output_fasta_file:
        output_fasta_file.write('>dataset_3\n')
        output_fasta_file.write(''.join(sorted_seq))

    log.info("All variants: %i" % all_variants)
    log.info("NA variants: %i" % na_variants)
    log.info("Filtered variants: %i" % filtered_variants)
    log.info("Valid variants: %i (%f)" % (valid_variants, valid_variants / all_variants))
    return dataset


def parser_7(csv_in, fasta_in, *args):
    """
    Parser for dataset 7.

    The original dataset is read using csv.reader(), each line is then
    processed to extract the required values.

    Due to the format of the original dataset, the insertion into the
    DataFrame is not straight forward.

    Further (due to the un-ordered listing within the original dataset)
    the resulting DataFrame has to be ordered by index before returned.
    """

    protein = SeqIO.read(fasta_in, "fasta")
    dataset = pd.DataFrame(columns=get_column_names())
    current_positions = []
    all_variants = 0
    valid_variants = 0
    na_variants = 0
    filtered_variants = 0
    with open(csv_in, 'r') as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter='\t', )

        # skip header
        next(tsv_in)

        for row in tsv_in:

            # split method
            tmp = row[0].split('-')
            position = tmp[0]
            substitution = tmp[1]

            score1 = row[1]
            if score1 == 'NA':
                na_variants += 1
            else:
                score1 = float(score1)
                valid_variants += 1

            # score2 = row[2]
            # if score2 != 'NA':
            #     score2 = float(score2)

            all_variants += 1

            if substitution == 'NA':
                filtered_variants += 1
                continue

            position = int(position)
            if position not in current_positions:
                current_positions += [position]
                dataset.set_value(position, 'position', position)
                dataset.set_value(position, 'wt', protein.seq[position])

            dataset.set_value(position, substitution, score1)

    log.info("All variants: %i" % all_variants)
    log.info("NA variants: %i" % na_variants)
    log.info("Filtered variants: %i" % filtered_variants)
    log.info("Valid variants: %i (%f)" % (valid_variants, valid_variants / all_variants))
    return dataset.sort_index()


def parser_8(csv_in, fasta_in, *args):
    """
    Parser for dataset 8.

    The original dataset is read using csv.reader(), each line is then
    processed to extract the required values.
    """

    dataset = pd.DataFrame(columns=get_column_names())
    offset = -2
    seq = ''
    current_positions = []
    all_variants = 0
    valid_variants = 0
    na_variants = 0
    filtered_variants = 0
    with open(csv_in, 'r') as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter=',')

        # skip header
        next(tsv_in)

        for row in tsv_in:

            # split method
            position = int(row[0])
            wt = row[1]
            substitution = row[2]
            score = row[17]

            if not score:
                score = 'NA'
                na_variants += 1
            else:
                score = float(score)
                valid_variants += 1

            if position not in current_positions:
                if wt == "*":
                    continue
                if len(current_positions) > 0 and int(current_positions[-1]) != position - 1:
                    log.warn("missing residue %i: adding X" % (position - 1))
                    current_positions += [position - 1]
                    seq += "X"
                    dataset.set_value(position - 1, 'position', position - 1 + offset)
                    dataset.set_value(position - 1, 'wt', 'X')
                    dataset.set_value(position - 1, '*', 'NA')
                    na_variants += 19
                    all_variants += 19
                current_positions += [position]
                seq += wt

            all_variants += 1

            dataset.set_value(position, 'position', position + offset)
            dataset.set_value(position, 'wt', wt)
            dataset.set_value(position, substitution, score)

    seq = list(seq)

    # this zips seq and current_positions and sorts seq
    sorted_y_idx_list = sorted(range(len(current_positions)), key=lambda x: current_positions[x])
    sorted_seq = [seq[i] for i in sorted_y_idx_list]

    with open(fasta_in, 'w') as output_fasta_file:
        output_fasta_file.write('>dataset_8\n')
        output_fasta_file.write(''.join(sorted_seq))

    log.info("All variants: %i" % all_variants)
    log.info("NA variants: %i" % na_variants)
    log.info("Filtered variants: %i" % filtered_variants)
    log.info("Valid variants: %i (%f)" % (valid_variants, valid_variants / all_variants))
    return dataset


def parser_11(txt_in, fasta_in, *args):
    # with open(txt_in, 'r') as text_in:
    #     save_path = '/Users/danielvitale/Documents/Bromberglab/dataset_11'
    #     complete_data_filename = os.path.join(save_path, 'dataset_11_supp2_parsed.csv')
    #     dataset_11_csv = open(complete_data_filename, 'w')
    #     with dataset_11_csv as csv_out:
    #         in_txt = csv.reader(text_in, delimiter = ' ')
    #         out_csv = csv.writer(csv_out)
    #         out_csv.writerows(in_txt)
    #     text_in.close()
    #     dataset_11_csv.close()
    #
    # save_path = '/Users/danielvitale/Documents/Bromberglab/dataset_11'
    # complete_data_filename = os.path.join(save_path, 'dataset_11_supp2_parsed.csv')
    # fasta_file = os.path.join(save_path, 'dataset_11_fasta.txt')
    # output_fasta_file = open(fasta_file, 'w')
    #
    # with open(complete_data_filename, 'r') as csv_in:
    #     csv_in = csv.reader(csv_in, delimiter=',')
    #     next(csv_in)
    #     seq = ">dataset_11\n"
    #     current_positions = []
    #
    #     for row in csv_in:
    #         position = row[0]
    #         wt = row[1]
    #
    #         if position not in current_positions:
    #             current_positions += [position]
    #             seq += wt
    #
    #         else:
    #             continue
    #
    #     output_fasta_file.write(seq)
    #     output_fasta_file.close()
    log.error("NOT IMPLEMENTED")


def parser_12(csv_in, fasta_in, *args):
    """
    Parser for dataset 12.

    The original dataset is read using csv.reader(), each line is then
    processed to extract the required values.

    Here regular expressions are used for extraction.

    Further (due to the un-ordered listing within the original dataset)
    the resulting DataFrame has to be ordered by index before returned.
    """

    dataset = pd.DataFrame(columns=get_column_names())
    seq = ''
    current_positions = []
    all_variants = 0
    valid_variants = 0
    na_variants = 0
    filtered_variants = 0
    with open(csv_in, 'r') as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter='\t')

        # skip header
        next(tsv_in)

        for row in tsv_in:

            # split method
            regepx_match = re.match(r'S([A-Z])([0-9]+)([A-Z,*])', row[0], re.M | re.I)
            wt = regepx_match.group(1)
            position = int(regepx_match.group(2))
            substitution = regepx_match.group(3)

            score = row[2]
            # nrbarcodes = row[1]
            # mutvalstd = row[3]

            if not score:
                score = 'NA'
                na_variants += 1
            else:
                score = float(score)
                valid_variants += 1

            if position not in current_positions:
                current_positions += [position]
                seq += wt

            all_variants += 1

            dataset.set_value(position, 'position', position)
            dataset.set_value(position, 'wt', wt)
            dataset.set_value(position, substitution, score)

    for position in [117,118]:
        log.warn("missing residue %i: adding X" % (position))
        current_positions += [position]
        seq += "X"
        dataset.set_value(position, 'position', position)
        dataset.set_value(position, 'wt', 'X')
        dataset.set_value(position, '*', 'NA')
        na_variants += 19
        all_variants += 19

    seq = list(seq)

    # this zips seq and current_positions and sorts seq
    sorted_y_idx_list = sorted(range(len(current_positions)), key=lambda x: current_positions[x])
    sorted_seq = [seq[i] for i in sorted_y_idx_list]

    with open(fasta_in, 'w') as output_fasta_file:
        output_fasta_file.write('>dataset_12\n')
        output_fasta_file.write(''.join(sorted_seq))

    log.info("All variants: %i" % all_variants)
    log.info("NA variants: %i" % na_variants)
    log.info("Filtered variants: %i" % filtered_variants)
    log.info("Valid variants: %i (%f)" % (valid_variants, valid_variants / all_variants))
    return dataset.sort_index()


def parser_13(csv_in, fasta_in, *args):
    """
    Parser for dataset 13.

    The original dataset is read using csv.reader(), each line is then
    processed to extract the required values.

    Here regular expressions are used for extraction.
    """

    dataset = pd.DataFrame(columns=get_column_names())
    seq = ''
    current_positions = []
    all_variants = 0
    valid_variants = 0
    na_variants = 0
    filtered_variants = 0
    with open(csv_in, 'r') as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter=',')

        # skip header
        next(tsv_in)

        for row in tsv_in:

            # split method
            regepx_match = re.match(r'([A-Z])([0-9]+)([A-Z,*])', row[0], re.M | re.I)
            wt = regepx_match.group(1)
            position = int(regepx_match.group(2))
            substitution = regepx_match.group(3)
            score = row[6]
            score2 = row[7]
            # score3 = row[8]

            if not score:
                score = 'NA'
                na_variants += 1
            else:
                score = float(score)
                valid_variants += 1

            if float(score2) > 0.3:
                filtered_variants += 1
                valid_variants -= 1
                score = 'NA'

            if position not in current_positions:
                current_positions += [position]
                seq += wt

            all_variants += 1

            dataset.set_value(position, 'position', position)
            dataset.set_value(position, 'wt', wt)
            dataset.set_value(position, substitution, score)

        seq = list(seq)

        # this zips seq and current_positions and sorts seq
        sorted_y_idx_list = sorted(range(len(current_positions)), key=lambda x: current_positions[x])
        sorted_seq = [seq[i] for i in sorted_y_idx_list]

        with open(fasta_in, 'w') as output_fasta_file:
            output_fasta_file.write('>dataset_13\n')
            output_fasta_file.write(''.join(sorted_seq))

        log.info("All variants: %i" % all_variants)
        log.info("NA variants: %i" % na_variants)
        log.info("Filtered variants: %i" % filtered_variants)
        log.info("Valid variants: %i (%f)" % (valid_variants, valid_variants / all_variants))
        return dataset


def parser_15(csv_in, fasta_in, *args):
    """
    Parser for dataset 15.
    duplicates of substitution scores are removed and only
    unique position/substitution combo scores are written
    to a new, cleaned, csv dataset file before parsing.

    """

# the following mines a fasta_in document from

    # the following cleans the csv file to remove duplicates to prep for parsing
    dataset = pd.DataFrame(columns=get_column_names())
    offset = -1
    seq = ''
    current_positions = []
    all_variants = 0
    valid_variants = 0
    na_variants = 0
    filtered_variants = 0
    with open(csv_in, 'r') as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter=',')
        next(tsv_in)
        next(tsv_in)

        pos_dict = {}

        for row in tsv_in:
            mut1_wt = row[0]
            mut1_position = int(row[1])
            mut1_mutation = row[2]
            mut2_wt = row[3]
            mut2_position = row[4]
            mut2_mutation = row[5]
            mut1_score = row[8]
            wt_single = row[12]
            position_single = row[13]
            if mut1_position not in pos_dict:
                pos_dict[mut1_position] = [mut1_mutation]
            else:
                if mut1_mutation not in pos_dict[mut1_position]:
                    pos_dict[mut1_position].append(mut1_mutation)
                else:
                    continue

            if not mut1_score:
                mut1_score = 'NA'
                na_variants += 1
            else:
                mut1_score = float(mut1_score)
                valid_variants += 1

            if mut1_position not in current_positions:
                if len(current_positions) > 0 and int(current_positions[-1]) != mut1_position - 1:
                    log.warn("missing residue %i: adding Y" % (mut1_position - 1))
                    current_positions += [mut1_position - 1]
                    seq += "Y"
                    dataset.set_value(mut1_position - 1, 'position', mut1_position - 1 + offset)
                    dataset.set_value(mut1_position - 1, 'wt', 'Y')
                    dataset.set_value(mut1_position - 1, '*', 'NA')
                    na_variants += 19
                    all_variants += 19
                current_positions += [mut1_position]
                seq += mut1_wt

            all_variants += 1

            dataset.set_value(mut1_position, 'position', mut1_position + offset)
            dataset.set_value(mut1_position, 'wt', mut1_wt)
            dataset.set_value(mut1_position, mut1_mutation, mut1_score)

    seq = list(seq)

    # this zips seq and current_positions and sorts seq
    sorted_y_idx_list = sorted(range(len(current_positions)), key=lambda x: current_positions[x])
    sorted_seq = [seq[i] for i in sorted_y_idx_list]

    with open(fasta_in, 'w') as output_fasta_file:
        output_fasta_file.write('>dataset_15\n')
        output_fasta_file.write(''.join(sorted_seq))

    log.info("All variants: %i" % all_variants)
    log.info("NA variants: %i" % na_variants)
    log.info("Filtered variants: %i" % filtered_variants)
    log.info("Valid variants: %i (%f)" % (valid_variants, valid_variants / all_variants))
    return dataset.sort_values(by='position')


def parser_17(csv_in, fasta_in, *args):
    """
    Parser for dataset 17.

    The original dataset is read using csv.reader(), each line is then
    processed to extract the required values.

    Here regular expressions are used for extraction.
    """

    protein = SeqIO.read(fasta_in, "fasta")
    seq_idx = 1
    dataset = pd.DataFrame(columns=get_column_names())
    current_positions = []
    all_variants = 0
    valid_variants = 0
    na_variants = 0
    filtered_variants = 0
    with open(csv_in, 'r') as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter=',')

        # skip header
        next(tsv_in)

        for row in tsv_in:

            # split method
            regepx_match = re.match(r'([A-Z])([0-9]+)([A-Z,*])', row[0], re.M | re.I)
            wt = regepx_match.group(1)
            position = int(regepx_match.group(2))
            substitution = regepx_match.group(3)

            if position != seq_idx:
                dataset.set_value(seq_idx, 'position', seq_idx)
                dataset.set_value(seq_idx, 'wt', protein.seq[seq_idx-1])
                #dataset.set_value(position, substitution, score)
                na_variants += 19
                all_variants += 19
                seq_idx += 1
                continue

            # score1 = row[1]
            # score2 = row[2]
            score = row[3]

            if not score:
                score = 'NA'
                na_variants += 1
            else:
                score = float(score)
                valid_variants += 1

            all_variants += 1

            dataset.set_value(position, 'position', position)
            dataset.set_value(position, 'wt', wt)
            dataset.set_value(position, substitution, score)

    log.info("All variants: %i" % all_variants)
    log.info("NA variants: %i" % na_variants)
    log.info("Filtered variants: %i" % filtered_variants)
    log.info("Valid variants: %i (%f)" % (valid_variants, valid_variants / all_variants))
    return dataset


def parser_22(txt_in, fasta_in, *args):
    # with open(txt_in, 'r') as text_in:
    #     save_path = '/Users/danielvitale/Documents/Bromberglab/dataset_22'
    #     complete_data_filename = os.path.join(save_path, 'dataset_22_supp_3_parsed.csv')
    #     dataset_22_csv = open(complete_data_filename, 'w')
    #     with dataset_22_csv as csv_out:
    #         in_txt = csv.reader(text_in, delimiter = ' ')
    #         out_csv = csv.writer(csv_out)
    #         out_csv.writerows(in_txt)
    #     text_in.close()
    #     dataset_22_csv.close()
    #
    # save_path = '/Users/danielvitale/Documents/Bromberglab/dataset_22'
    # complete_data_filename = os.path.join(save_path, 'dataset_22_supp_3_parsed.csv')
    # fasta_file = os.path.join(save_path, 'dataset_22_supp_3_fasta.txt')
    # output_fasta_file = open(fasta_file, 'w')
    #
    # with open(complete_data_filename, 'r') as csv_in:
    #     csv_in = csv.reader(csv_in, delimiter=',')
    #     next(csv_in)
    #     seq = ">dataset_22\n"
    #     current_positions = []
    #
    #     for row in csv_in:
    #         position = row[0]
    #         wt = row[1]
    #
    #         if position not in current_positions:
    #             current_positions += [position]
    #             seq += wt
    #
    #         else:
    #             continue
    #
    #     output_fasta_file.write(seq)
    #     output_fasta_file.close()
    log.error("NOT IMPLEMENTED")


def parser_24(csv_in, fasta_in, *args):
    protein = SeqIO.read(fasta_in, "fasta")
    seq_idx = 1
    dataset = pd.DataFrame(columns=get_column_names())
    seq = ''
    current_positions = []
    all_variants = 0
    valid_variants = 0
    na_variants = 0
    filtered_variants = 0
    with open(csv_in) as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter=',')

        next(tsv_in)

        for row in tsv_in:
            regepx_match = re.match(r'([A-Z,_])([0-9]+)([A-Z,*,_])', row[2], re.M | re.I)
            wt = regepx_match.group(1)
            position = int(regepx_match.group(2))
            substitution = regepx_match.group(3)
            #score = row[1]
            score = row[3]

            if score == 'NA':
                score = 'NA'
            else:
                score = float(score)

            dataset.set_value(position, 'position', position)
            dataset.set_value(position, 'wt', wt)
            dataset.set_value(position, substitution, score)

    seq = list(seq)

    # this zips seq and current_positions and sorts seq
    sorted_y_idx_list = sorted(range(len(current_positions)), key=lambda x: current_positions[x])
    sorted_seq = [seq[i] for i in sorted_y_idx_list]

    with open(fasta_in, 'w') as output_fasta_file:
        output_fasta_file.write('>dataset_15\n')
        output_fasta_file.write(''.join(sorted_seq))

    log.info("All variants: %i" % all_variants)
    log.info("NA variants: %i" % na_variants)
    log.info("Filtered variants: %i" % filtered_variants)
    log.info("Valid variants: %i (%f)" % (valid_variants, valid_variants / all_variants))
    return dataset


def parser_25(csv_in, fasta_in, *args):
    dataset = pd.DataFrame(columns=get_column_names())
    with open(csv_in) as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter=',')

def parser_27(csv_in, fasta_in, *args):
    dataset = pd.DataFrame(columns=get_column_names())
    with open(csv_in) as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter=',')
        save_path = '/Users/danielvitale/Documents/Bromberglab/dataset_27'
        complete_fasta_filename = os.path.join(save_path, 'dataset_27_seq.fasta.txt')
        output_fasta_file = open(complete_fasta_filename, 'w')

        seq = ''
        current_positions = []
        next(tsv_in)

        for row in tsv_in:
            print(row)
            regepx_match = re.match(r'([A-Z,_])([0-9]+)([A-Z*_]*)', row[0], re.M | re.I)
            wt = regepx_match.group(1)
            position = int(regepx_match.group(2))
            substitution = regepx_match.group(3)

            if substitution == "STOP":
                continue

            if position not in current_positions:
                current_positions += [position]
                seq += wt
            else:
                continue
        seq = list(seq)

        # this zips seq and current_positions and sorts seq
        sorted_y_idx_list = sorted(range(len(current_positions)), key=lambda x: current_positions[x])
        Xs = [seq[i] for i in sorted_y_idx_list]

        seq1 = '>dataset_27 fasta\n'
        seq1 = seq1 + ''.join(Xs)

        output_fasta_file.write(seq1)
        output_fasta_file.close()

    new_fasta = open(complete_fasta_filename, 'r')
    protein = SeqIO.read(new_fasta, "fasta")
    dataset = pd.DataFrame(columns=get_column_names())
    current_positions = []
    with open(csv_in) as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter=',')

        next(tsv_in)
        for row in tsv_in:


            regepx_match = re.match(r'([A-Z,_])([0-9]+)([A-Z*_]*)', row[0], re.M | re.I)
            wt = regepx_match.group(1)
            position = int(regepx_match.group(2))
            substitution = regepx_match.group(3)
            score = row[1]

            if substitution == "STOP":
                continue
            if score == 'NA':
                score = 'NA'
            if score == ' --':
                score = 'NA'
            else:
                score = float(score)

            dataset.set_value(position, 'position', position)
            dataset.set_value(position, 'wt', wt)
            dataset.set_value(position, substitution, score)

    return dataset

def parser_42(csv_in, fasta_in, *args):
    dataset = pd.DataFrame(columns=get_column_names())
    with open(csv_in) as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter='\t')
        save_path = '/Users/danielvitale/Documents/Bromberglab/dataset_42'
        complete_fasta_filename = os.path.join(save_path, 'dataset_42_seq.fasta.txt')
        output_fasta_file = open(complete_fasta_filename, 'w')

        seq = ''
        current_positions = []
        idx = 0
        for row in tsv_in:
            if idx < 6:
                idx += 1
                continue
            regepx_match = re.match(r'([A-Z])([0-9]+)([A-Z,*])', row[0], re.M | re.I)
            wt = regepx_match.group(1)
            position = int(regepx_match.group(2))

            if position not in current_positions:
                current_positions += [position]
                seq += wt
            else:
                continue
        seq = list(seq)

        # this zips seq and current_positions and sorts seq
        sorted_y_idx_list = sorted(range(len(current_positions)), key=lambda x: current_positions[x])
        Xs = [seq[i] for i in sorted_y_idx_list]

        seq1 = '>dataset_42 fasta\n'
        seq1 = seq1 + ''.join(Xs)


        output_fasta_file.write(seq1)
        output_fasta_file.close()

    new_fasta = open(complete_fasta_filename, 'r')
    protein = SeqIO.read(new_fasta, "fasta")
    dataset = pd.DataFrame(columns=get_column_names())
    current_positions = []
    with open(csv_in) as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter=',')

        idx = 0
        for row in tsv_in:
            if idx < 6:
                idx += 1
                continue

            regepx_match = re.match(r'([A-Z])([0-9]+)([A-Z,*])', row[0], re.M | re.I)
            wt = regepx_match.group(1)
            position = int(regepx_match.group(2))
            substitution = regepx_match.group(3)
            score = row[2]

            if not score:
                score = 'NA'
            else:
                score = float(score)

            dataset.set_value(position, 'position', position)
            dataset.set_value(position, 'wt', wt)
            dataset.set_value(position, substitution, score)

    return dataset

def parser_dist_analysis(csv_in, fasta_in, *args):
    with open(csv_in) as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter='\t')
        save_path = '/Users/danielvitale/Documents/Bromberglab/dataset_42'
        complete_fasta_filename = os.path.join(save_path, 'dataset_42_seq.fasta.txt')
        output_fasta_file = open(complete_fasta_filename, 'w')
        seq = ''
        current_positions = []
        idx = 0
        for row in tsv_in:
            if idx < 4:
                idx += 1
                continue
            regepx_match = re.match(r'([A-Z])([0-9]+)([A-Z,*])', row[0], re.M | re.I)
            wt = regepx_match.group(1)
            position = int(regepx_match.group(2))

            if position not in current_positions:
                current_positions += [position]
                seq += wt
            else:
                continue
        seq = list(seq)

        # this zips seq and current_positions and sorts seq
        sorted_y_idx_list = sorted(range(len(current_positions)), key=lambda x: current_positions[x])
        Xs = [seq[i] for i in sorted_y_idx_list]

        seq1 = '>dataset_42 fasta\n'
        seq1 = seq1 + ''.join(Xs)

        output_fasta_file.write(seq1)
        output_fasta_file.close()

    new_fasta = open(complete_fasta_filename, 'r')
    protein = SeqIO.read(new_fasta, "fasta")
    dataset = pd.DataFrame(columns=get_column_names())
    current_positions = []
    with open(csv_in) as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter=',')

        idx = 0
        for row in tsv_in:
            if idx < 4:
                idx += 1
                continue

            regepx_match = re.match(r'([A-Z])([0-9]+)([A-Z,*])', row[0], re.M | re.I)
            wt = regepx_match.group(1)
            position = int(regepx_match.group(2))
            substitution = regepx_match.group(3)
            score = row[1]

            if not score:
                score = 'NA'
            else:
                score = float(score)

            dataset.set_value(position, 'position', position)
            dataset.set_value(position, 'wt', wt)
            dataset.set_value(position, substitution, score)

    return dataset

def som_rheostat_parser(csv_in, fasta_in, *args):
    with open(csv_in) as tsv_in:
        tsv_in = csv.reader(tsv_in, delimiter='\t')
        save_path = '/Users/danielvitale/Documents/Bromberglab/R_distribution_analysis'
        complete_fasta_filename = os.path.join(save_path, 'toggle_normed_parsed.csv')
        output_fasta_file = open(complete_fasta_filename, 'w')
        seq = ''
        current_positions = []
        idx = 0
        for row in tsv_in:
            if idx < 1:
                idx += 1
                continue
            regepx_match = re.match(r'([A-Z])([0-9]+)([A-Z,*])', row[1], re.M | re.I)
            wt = regepx_match.group(1)
            position = int(regepx_match.group(2))

            if position not in current_positions:
                current_positions += [position]
                seq += wt
            else:
                continue

def profparser(csv_in, fasta_in, *args):




    with open(csv_in) as tsv_in:
        tsv_in = pd.read_csv(tsv_in, usecols=[0,1,2])
        tsv_in = tsv_in.set_index('no')
        save_path = '/Users/danielvitale/Documents/Bromberglab/practice'
        complete_out_filename = os.path.join(save_path, 'outmap_parsed.csv')
        output_file = open(complete_out_filename, 'w')
        tsv_in.to_csv(output_file)


        #tsv_in = tsv_in.to_dict("index")
        #print(tsv_in)


        #import subprocess
        #HOST = 'VirtualBox'
        #COMMAND = 'start -a'
        #proc = subprocess.Popen('VirtualBox', )
        #result = proc.stdout.readlines()















        #tsv_in = csv.reader(tsv_in, delimiter=',')

        #tsv_in = tsv_in.groupby('aa')['aa','phel', 'ri_s'].apply(
         #   lambda x: x.set_index('aa', 'phel', 'ri_s').to_dict(orient='index')).to_dict()

        #res = {k: gp.to_dict(orient='index')
         #      for k, gp in tsv_in.set_index('aa', append=True).groupby(level=1)}



        #print(res)
        #print(df)


        #positions = []
        #headers = []
        #vals = []
        #row_num = 0

        #for row in tsv_in:
         #   position = row[0]

          #  if position not in positions:
           #     positions.append(position)

#            row_num += 1
 #           for val in row:
  #              if row_num == 1:
   #                 headers.append(val)
        #for row in tsv_in:

        #d = {}



    #p = subprocess.Popen('ls', shell = True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    #for line in p.stdout.readlines():
     #   print(line)

                
def get_argument_parser(dms_parser):
    """Argument parser if module is called from command line, see main method."""

    parser = argparse.ArgumentParser(description='Collection of parsing routines for Deep Mutational Scanning datasets')
    parser.add_argument('query', metavar='query.fasta',
                        help='query sequence file in fasta format')
    parser.add_argument('dataset', metavar='dataset.csv',
                        help='experimental deep sequencing dataset in csv or tsv format')
    parser.add_argument('parser', metavar='parser_id',
                        help='id of the available parsers to use allowed values are: %s' % ', '.join(
                            '%s:[%s]' % (id, dms_parser.parser_index.get(id)[1]) for id in dms_parser.parser_index.keys()))
    parser.add_argument('-o', '--output', metavar='output_parsed.csv',
                        help='Writes the parsed dataset to the given output file')
    return parser


if __name__ == "__main__":
    DMSParser = DMSDatasetParser()
    args = get_argument_parser(DMSParser).parse_args()
    args.query = os.path.abspath(args.query)
    args.dataset = os.path.abspath(args.dataset)

    data = DMSParser.parse_dataset(args.parser, args.dataset, args.query)
    if args.output is not None:
        DMSParser.log.info('writing parsed dataset to file: %s' % os.path.abspath(args.output))
        data.to_csv(os.path.abspath(args.output), index=False, na_rep="NA")
