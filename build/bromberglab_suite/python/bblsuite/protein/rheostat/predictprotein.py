import os
import pandas as pd
import numpy as np
from bblsuite.helper import logger

class Parser:
    """Rostlab PredictProtein Interface"""

    log = logger.Logger.get_logger()

    def __init__(self, pprotein_db="/mnt/pprotein"):
        self.pprotein_db = pprotein_db

    def consurf(self, fasta_in, query_sequence, force_run=False):
        """Parse conservation scores from consurf output"""

        # consurf removes any occurences of "X" in the query sequence;
        # this is needed for correct index-based merging of the result dataframe
        query_sequence_index = []
        idx = 1
        for residue in query_sequence:
            if residue != "X":
                query_sequence_index.append(idx)
            idx += 1

        filename = os.path.splitext(os.path.basename(fasta_in))[0]
        basepath = os.path.dirname(fasta_in)
        raw = os.path.join(os.path.join(self.pprotein_db, filename), "query" + ".consurf.grades")

        parsed = os.path.join(basepath, filename + '.consurf_pprotein.parsed.csv')

        if not os.path.exists(raw):
            self.log.warn("consurf_pprotein: could not find consurf file...")
        elif force_run:
            self.log.error("consurf_pprotein: could not find consurf file...")

        self.log.info("consurf_pprotein: parsing from preditcprotein...")

        with open(raw) as csv_in:
            # POS	 SEQ	SCORE		COLOR	CONFIDENCE INTERVAL	CONFIDENCE INTERVAL COLORS	MSA DATA	RESIDUE VARIETY
            #   	(normalized)
            df = pd.read_csv(
                csv_in,
                engine='python',
                sep='\t+',
                skipinitialspace=True,
                comment="#",
                header=None,
                skiprows=14,
                skipfooter=4,
                names=['POS', 'SEQ', 'SCORE', 'COLOR', 'CONFIDENCE INTERVAL', 'CONFIDENCE INTERVAL COLORS', 'empty', 'MSA DATA', 'RESIDUE VARIETY'],
                usecols=['POS', 'SEQ', 'SCORE']
            )
            df.SEQ = df.SEQ.str.strip()
            df.rename(columns={'POS': 'position'}, inplace=True)
            df.rename(columns={'SEQ': 'WT_aa'}, inplace=True)
            df.rename(columns={'SCORE': 'consurf_score'}, inplace=True)
            df = df.set_index('position')
            df.index = query_sequence_index
            df.index.name = 'position'
            output_file = open(parsed, 'w')
            df.to_csv(output_file)
            output_file.close()

        return df

    def mdisorder(self, fasta_in, force_run=False):
        """Parse mdisorder output"""

        filename = os.path.splitext(os.path.basename(fasta_in))[0]
        basepath = os.path.dirname(fasta_in)
        raw = os.path.join(os.path.join(self.pprotein_db, filename), "query" + ".mdisorder")

        parsed = os.path.join(basepath, filename + '.mdisorder_pprotein.parsed.csv')

        if not os.path.exists(raw):
            self.log.warn("mdisorder_pprotein: could not find mdisorder file...")
        elif force_run:
            self.log.error("mdisorder_pprotein: could not find mdisorder file...")

        self.log.info("mdisorder_pprotein: parsing from predictprotein...")

        with open(raw) as csv_in:
            # Number Residue NORSnet NORS2st PROFbval bval2st Ucon Ucon2st MD_raw   MD_rel  MD2st
            df = pd.read_csv(
                csv_in,
                engine='python',
                sep='\t+',
                skipinitialspace=True,
                comment="#",
                skiprows=1,
                skipfooter=15,
                names=['Number', 'Residue', 'NORSnet', 'NORS2st', 'PROFbval', 'bval2st', 'Ucon', 'Ucon2st', 'MD_raw', 'MD_rel', 'MD2st'],
                usecols=['Number', 'Residue', 'NORSnet', 'PROFbval', 'Ucon', 'MD_raw', 'MD_rel']
            )
            df.rename(columns={'Number': 'position'}, inplace=True)
            df.rename(columns={'Residue': 'residue'}, inplace=True)
            df = df.set_index('position')
            df.index = np.arange(1, len(df.index) + 1)
            df.index.name = 'position'
            output_file = open(parsed, 'w')
            df.to_csv(output_file)
            output_file.close()

        return df

    def somena(self, fasta_in, force_run=False):
        """Parse somena output"""

        filename = os.path.splitext(os.path.basename(fasta_in))[0]
        basepath = os.path.dirname(fasta_in)
        raw = os.path.join(os.path.join(self.pprotein_db, filename), "query" + ".somena")

        parsed = os.path.join(basepath, filename + '.somena_pprotein.parsed.csv')

        if not os.path.exists(raw):
            self.log.warn("somena_pprotein: could not find mdisorder file...")
        elif force_run:
            self.log.error("somena_pprotein: could not find mdisorder file...")

        self.log.info("somena_pprotein: parsing from predictprotein...")

        with open(raw) as csv_in:
            # Number Residue NORSnet NORS2st PROFbval bval2st Ucon Ucon2st MD_raw   MD_rel  MD2st
            df = pd.read_csv(
                csv_in,
                sep='\t',
                comment="#",
                skiprows=40,
                usecols=['NO', 'RES', 'DNA_AVG', 'DNA_JURY', 'RNA_AVG', 'RNA_JURY', 'XNA_AVG', 'XNA_JURY']
            )
            df.rename(columns={'NO': 'position'}, inplace=True)
            df.rename(columns={'RES': 'residue'}, inplace=True)
            df = df.set_index('position')
            df.index = np.arange(1, len(df.index) + 1)
            df.index.name = 'position'
            output_file = open(parsed, 'w')
            df.to_csv(output_file)
            output_file.close()

        return df

    def profbval4snap(self, fasta_in, force_run=False):
        """Parse profbval output"""

        filename = os.path.splitext(os.path.basename(fasta_in))[0]
        basepath = os.path.dirname(fasta_in)
        raw = os.path.join(os.path.join(self.pprotein_db, filename), "query" + ".profb4snap")

        parsed = os.path.join(basepath, filename + '.bval_parsed.parsed.csv')

        if not os.path.exists(raw):
            self.log.warn("profbval4snap_pprotein: could not find profb4snap file...")
        elif force_run:
            self.log.error("profbval4snap_pprotein: could not find profb4snap file...")

        self.log.info("profbval4snap_pprotein: parsing from predictprotein...")

        # number residue PREL acc Bnorm NS RI_ns S RI_s output

        with open(raw) as new_csv_in:
            df = pd.read_csv(new_csv_in, delim_whitespace=True, usecols=['number', 'residue', 'acc', 'Bnorm'])
            df.rename(columns={'number': 'position'}, inplace=True)
            df.rename(columns={'residue': 'WT_aa'}, inplace=True)
            df = df.set_index('position')
            df.index = pd.to_numeric(df.index)
            output_file = open(parsed, 'w')
            df.to_csv(output_file)
            output_file.close()

        return df

    def profRdb(self, fasta_in, force_run=False):
        """Parse profRdb output"""

        filename = os.path.splitext(os.path.basename(fasta_in))[0]
        basepath = os.path.dirname(fasta_in)
        raw = os.path.join(os.path.join(self.pprotein_db, filename), "query" + ".profRdb")

        parsed = os.path.join(basepath, filename + '.prof_pprotein.parsed.csv')

        if not os.path.exists(raw):
            self.log.warn("profRdb_pprotein: could not find profRdb file...")
        elif force_run:
            self.log.error("profRdb_pprotein: could not find profRdb file...")

        self.log.info("profRdb_pprotein: parsing from predictprotein...")


        with open(raw) as csv_in:
            # No	AA	OHEL	PHEL	RI_S	OACC	PACC	OREL	PREL	RI_A	pH	pE	pL	Obe	Pbe	Obie	Pbie	OtH	OtE	OtL	Ot0	Ot1	Ot2	Ot3	Ot4	Ot5	Ot6	Ot7	Ot8	Ot9
            df = pd.read_csv(
                csv_in,
                sep='\t',
                comment="#",
                usecols=['No', 'AA', 'PACC', 'pH', 'pE', 'pL'] # 'PHEL', 'RI_S', 'PACC', 'PREL', 'RI_A', 'pH', 'pE', 'pL', 'Pbie']
            )
            df.rename(columns={'No': 'position'}, inplace=True)
            df.rename(columns={'AA': 'WT_aa'}, inplace=True)
            df = df.set_index('position')
            df.index = pd.to_numeric(df.index)
            output_file = open(parsed, 'w')
            df.to_csv(output_file)
            output_file.close()

        return df