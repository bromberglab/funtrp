#!/usr/bin/env python3
import argparse
import math
import os
import csv
import subprocess
import pandas as pd
import numpy as np
from Bio import SeqIO
from weka.core import jvm, converters, serialization
from weka.core.classes import Random
from weka.core.dataset import Instances
from weka.filters import Filter
from weka.classifiers import PredictionOutput, Classifier, KernelClassifier, FilteredClassifier, Kernel, Evaluation
from weka.attribute_selection import ASSearch, ASEvaluation, AttributeSelection
from bblsuite.protein.aminoacid import aminoacid_main
from bblsuite.protein.rheostat import methods
from bblsuite.helper import logger

__author__ = 'mmiller'
__email__ = 'mmiller@bromgberglab.org'


log = logger.Logger.get_logger(console=False)
MIN_EXPSCORES_THRESHOLD = 6


def get_residue_stats(pos, dataset: pd.DataFrame):
    return dataset.loc[dataset['position_mapping'] == pos]


def extract_features(query_id, query_fa, experimental_set, suffix=None, labels=None, mapping=None):
    log.info('extracting features...')
    filename = os.path.splitext(os.path.basename(query_fa))[0]
    basepath = os.path.dirname(query_fa)

    if suffix:
        query_suffix = suffix
        query_fa = os.path.join(basepath, filename + query_suffix + ".fa")
    else:
        query_suffix = ""

    methods.read_query_sequence(query_fa, force_read=True)

    methods.psiblast_run(query_fa,
                         cmd_extension='-evalue 1e-3 '
                                       '-inclusion_ethresh 1e-3 '
                                       '-num_iterations 3 '
                                       '-max_target_seqs 10000 '
                                       '-seg no -use_sw_tback',
                         fasta_eval_threshold=10**-3,
                         #suffix=".full",
                         force_run=False
                         )
    feature_set = list()
    # prof = methods.prof_wrapper(query_fa, force_run=False)
    # feature_set.append(prof)
    # profbval = methods.profbval_wrapper(query_fa, suffix=query_suffix, force_run=False)
    # feature_set.append(profbval)
    # tmseg = methods.tmseg_wrapper(query_fa, suffix=query_suffix, force_run=False)
    # feature_set.append(tmseg)
    # methods.psiblast_run(query_fa, cmd_extension='-evalue 1e-5 -inclusion_ethresh 1e-3 -num_iterations 1 -max_target_seqs 10000 -seg no -use_sw_tback', suffix=".custom", force_run=False)
    # methods.psiblast_run(query_fa, cmd_extension='-evalue 1e-3 -inclusion_ethresh 1e-3 -num_iterations 3 -seg no -use_sw_tback')
    mafft = methods.mafft_wrapper(query_fa, force_run=False)
    # rate4site = methods.conservation(query_fa, method='rate4site', force_run=False)
    # consCalc = methods.conservation(query_fa, method='consCalc', force_run=True)
    # feature_set.append(consCalc)
    # evotrace = methods.conservation(query_fa, method='evotrace', force_run=False)
    # feature_set.append(evotrace)
    pprotein_rdbprof = methods.pprotein_parser.profRdb(query_fa)
    feature_set.append(pprotein_rdbprof)
    pprotein_consurf = methods.pprotein_parser.consurf(query_fa, methods.query_sequence_obj.seq)
    feature_set.append(pprotein_consurf)
    pprotein_mdisorder = methods.pprotein_parser.mdisorder(query_fa)
    feature_set.append(pprotein_mdisorder)
    frequencies = methods.msa_frequencies(query_fa)
    feature_set.append(frequencies[0])
    aavectors = methods.aa_vectors(query_fa)
    feature_set.append(aavectors)
    # snap2aucr = methods.snap2aucr(query_fa, query_id)
    # feature_set.append(snap2aucr)
    # possible_snps = methods.possible_snps(query_fa)
    # feature_set.append(possible_snps)
    # xchange = methods.xchange(query_fa)
    # feature_set.append(xchange)
    basic_properties = methods.aa_basic_prop(query_fa)
    feature_set.append(basic_properties)
    # somena = methods.pprotein_parser.somena(query_fa)
    # feature_set.append(somena)
    possible_snps_count = methods.possible_snps(query_fa)
    possible_snps_count['possible_snps'] = possible_snps_count['possible_snps'].apply(lambda x: len(x))
    feature_set.append(possible_snps_count)

    features = pd.DataFrame(pd.concat(feature_set, axis=1, join_axes=[feature_set[0].index]))

    if mapping is not None:
        features = features.iloc[mapping]

    if experimental_set is not None:
        features.index = experimental_set.index
        features = pd.concat([experimental_set, features], axis=1)
        features = features.loc[:, ~features.columns.duplicated()]
        # features = features.drop(
        #     ['WT_aa', 'scores', 'score_substitutions', 'na_substitutions', 'SEG', 'TOP', 'wt_score', #'variability',
        #      'position_mapping', 'possible_snps', 'score_notSNPpossible'], axis=1)
        features = features.drop(
            ['WT_aa', 'scores', 'score_substitutions', 'na_substitutions', 'wt_score',
             'position_mapping', 'score_notSNPpossible'], axis=1)
        features = features.drop(
            ['NORSnet', 'Ucon', 'MD_rel', 'hydrophobic', 'hydrophilic', 'aromatic', 'non-polar aliphatic'], axis=1)
        features_ = features.residue
        features = features.drop(['residue'], axis=1)
        features.insert(2, 'residue', features_)
    else:
        features = features.drop(
            ['position', 'NORSnet', 'Ucon', 'MD_rel', 'hydrophobic', 'hydrophilic', 'aromatic', 'non-polar aliphatic'], axis=1)
        features.rename(columns={'WT_aa': 'residue'}, inplace=True)
        features = features.loc[:, ~features.columns.duplicated()]
        positions = list()
        raucs = list()
        for idx in np.arange(1, len(features.index) + 1):
            positions.append("%s:%i" % (query_id, idx))
            raucs.append(0.0)
        features.insert(0, 'position', positions)
        features.insert(1, 'rauc', raucs)
        #features.insert(7, 'consurf_score', raucs)

    if labels is None:
        if experimental_set is not None:
            label_char = '?'
        else:
            label_char = 'X'
        labels = pd.DataFrame(columns=['label'])
        for idx in range(1, len(feature_set[0].index) + 1):
            labels.loc[idx] = label_char

    features = features.assign(label=labels.iloc[:, 0].values)

    return features


class Training:

    def __init__(self, debug=False):
        log.debug('created training instance')
        self.debug = debug

    def set_query(self, query_id, basepath=None, skip=False, extend=False, evaluate=False, evlmodel='RT'):
        self.dataset = None
        self.statistics = None
        self.features = None
        self.query_id = query_id
        self.query_fa = os.path.join(basepath, query_id + ".fa")
        self.fin_experimental = os.path.join(basepath, query_id + ".variants_experimental.csv")
        self.fout_statistics = os.path.join(basepath, query_id + ".experimental.csv")
        self.fout_features = os.path.join(basepath, query_id + ".features.csv")
        self.fout_features_extended = os.path.join(basepath, query_id + ".features_extended.csv")
        self.fout_evaluation = os.path.join(basepath, query_id + ".features_evaluation.csv")
        self.skip = skip
        self.extend = extend
        self.evaluate = evaluate
        self.model_id = evlmodel

    def read_dataset(self, csv_in):
        """Read dataset which has been parsed and exported earlier."""

        self.dataset = pd.read_csv(csv_in, dtype={'position': np.int})

    def filter_possible_SNPS(self):
        all_aa = set(aminoacid_main.Aminoacid.alphabet)
        for index,row in self.dataset.iterrows():
            wt = row[1]
            possible_snps = set(aminoacid_main.Aminoacid(wt).get_possible_snps())
            impossible_aa = list(all_aa.difference(possible_snps))
            for aa in impossible_aa:
                self.dataset.loc[index, aa] = np.nan

    def analyze_dataset(self, fasta_in):
        """Analyze dataset and return DataFrame containing statistics per residue."""

        protein = SeqIO.read(fasta_in, "fasta")
        protein_sequence = protein.seq
        protein_id = protein.id
        log.info("experimental sequence: id %s length %i" % (protein_id, len(protein_sequence)))

        self.statistics = pd.DataFrame(
            columns=['position',
                     'residue',
                     'wt_score',
                     'position_mapping',
                     'scores',
                     'score_substitutions',
                     'score_notSNPpossible',
                     'na_substitutions']
        )

        for position, residue in enumerate(protein_sequence):
            dataset_row = self.dataset.iloc[position, ].tolist()
            dataset_position = dataset_row[0]
            dataset_aminoacid = dataset_row[1]

            assert dataset_aminoacid == residue

            if dataset_aminoacid == "X":
                log.debug("skipping residue %s at position %i" % (residue, position))
                continue

            position_mapping = dataset_position

            wildtype_score_available = True
            na_substitutions = []
            scores = []
            score_substitutions = []
            score_notSNPpossible = []

            for aa in aminoacid_main.Aminoacid.get_all_one_letter():
                if aa == "*":
                    continue

                aa_index = self.dataset.columns.get_loc(aa)
                score = dataset_row[aa_index]

                aa_obj = aminoacid_main.Aminoacid(dataset_aminoacid)
                if math.isnan(score):
                    na_substitutions += [aa]
                    if aa == dataset_aminoacid:
                        wildtype_score_available = False
                elif aa in aa_obj.get_possible_snps():
                    score_substitutions += [aa]
                    score = float(score)
                    scores += [score]
                else:
                    score = float(score)
                    score_notSNPpossible += [score]

            self.statistics = self.statistics.append(pd.DataFrame(
                [(position,
                  dataset_aminoacid,
                  wildtype_score_available,
                  position_mapping,
                  scores,
                  score_substitutions,
                  score_notSNPpossible,
                  na_substitutions)],
                columns=['position',
                         'residue',
                         'wt_score',
                         'position_mapping',
                         'scores',
                         'score_substitutions',
                         'score_notSNPpossible',
                         'na_substitutions'],
                index=[position])
            )

        #self.statistics['position'] = self.statistics['position'].astype(np.int)
        self.statistics['position'] = np.arange(1, len(self.statistics) + 1)
        self.statistics.index = np.arange(1, len(self.statistics) + 1)
        self.statistics['position_mapping'] = self.statistics['position_mapping'].astype(np.int)

        methods.aucr(self.statistics, self.query_id)

        log.info('removing all instances with wildtype "X" from experimental dataset...')
        self.dataset = self.dataset[~self.dataset.wt.isin(['X'])].reset_index(drop=True)

    def add_rt_labels(self, labels, df=None):
        """Add rheostat toggle labels to dataset"""

        if df is not None:
            df["rt_label"] = np.nan
        else:
            self.statistics["rt_label"] = np.nan
        for pos, label in labels.items():
            idx = get_residue_stats(pos, self.statistics).index
            if df is not None:
                df.set_value(idx, 'rt_label', label)
            else:
                self.statistics.set_value(idx, 'rt_label', label)

    def expand_features(self, suffix=None, mapping=None):

        query_fa = self.query_fa
        filename = os.path.splitext(os.path.basename(query_fa))[0]
        basepath = os.path.dirname(query_fa)
        if suffix:
            query_fa = os.path.join(basepath, filename + suffix + ".fa")

        residues = self.features['residue']

        # drop class column
        labels = self.features['label']
        self.features = self.features.drop(['label'], axis=1)

        # relabel
        # labels = methods.label_by_cluster(self.dataset, self.query_id, MIN_EXPSCORES_THRESHOLD)[['label']]
        # labels.index = self.features.index
        # self.features.insert(len(self.features.columns), 'label', labels)

        # ADD NEW FEATURES >>>>>>>>

        # mdisorder = methods.pprotein(query_fa, 'mdisorder')
        # new_features = mdisorder
        new_features = None

        # SOMENA
        # somena = methods.pprotein(query_fa, 'somena')
        # new_features = somena #pd.DataFrame(pd.concat([new_features, somena], axis=1))

        # SNP POSSIBLE COUNT
        possible_snps = methods.possible_snps(query_fa)
        possible_snps['possible_snps'] = possible_snps['possible_snps'].apply(lambda x: len(x))
        new_features = possible_snps['possible_snps'] #pd.DataFrame(pd.concat([new_features, somena], axis=1))

        # END <<<<<<<<<<<<<<<<<<<<<

        if mapping is not None:
            new_features = new_features.iloc[mapping]

        new_features.index = self.features.index

        # add to existing features
        self.features = pd.DataFrame(pd.concat([self.features, new_features], axis=1))

        # remove duplicates
        self.features = self.features.loc[:, ~self.features.columns.duplicated()]

        # insert residue
        # self.features.insert(len(self.features.columns), 'residue_single', residues)

        # insert class column
        self.features.insert(len(self.features.columns), 'label', labels)

    def filter_features(self, to_remove_list):
        self.features = self.features.drop(to_remove_list, axis=1)

    def run(self):

        if not self.skip or self.extend:
            # MAPPING
            basepath = os.path.dirname(self.query_fa)
            mapping_file = os.path.join(basepath, 'mapping')
            mapping_list = list()
            with open(mapping_file, 'r') as f:
                mapping_in = f.readline().strip()
                for mapped in mapping_in.split(','):
                    sub_, full_ = mapped.split(":")
                    sub_start, sub_end = sub_.split("-")
                    full_start, full_end = full_.split("-")
                    mapping_list.append(((int(sub_start), int(sub_end)), (int(full_start), int(full_end))))
            mapped_ranges = ([], [])
            for mapping in mapping_list:
                sub_ = mapped_ranges[0]
                sub_ += list(range(mapping[0][0] - 1, mapping[0][1]))
                full_ = mapped_ranges[1]
                full_ += list(range(mapping[1][0] - 1, mapping[1][1]))

        features_training = self.fout_features
        if self.skip:
            log.info("skipping feature extraction...")
            self.features = pd.read_csv(self.fout_features)
            self.statistics = pd.read_csv(self.fout_statistics)
        else:
            log.info('parsing dataset...')
            self.read_dataset(self.fin_experimental)

            log.info('analyzing dataset...')
            self.analyze_dataset(self.query_fa)
            if self.debug:
                log.info('writing dataset statistics...')
                self.statistics.to_csv(self.fout_statistics, index=False, na_rep="NA")

            # FEATURES : extract features
            self.filter_possible_SNPS()
            labels = methods.label_by_cluster(self.dataset.iloc[mapped_ranges[0]], self.query_id, MIN_EXPSCORES_THRESHOLD)[['label']]
            log.info("#? %i, #U %i, #T %i, #R %i, #N %i" %
                  (
                      len(labels[labels.label == "?"]),
                      len(labels[labels.label == "U"]),
                      len(labels[labels.label == "T"]),
                      len(labels[labels.label == "R"]),
                      len(labels[labels.label == "N"])
                   )
            )

            self.features = extract_features(self.query_id, self.query_fa, self.statistics.iloc[mapped_ranges[0]], ".full", labels, mapped_ranges[1])
    
            log.info('writing features...')
            self.features.to_csv(self.fout_features, index=False, na_rep="NA")
    
        if self.extend:
            features_training = self.fout_features_extended
            if self.skip:
                self.read_dataset(self.fin_experimental)
                self.filter_possible_SNPS()
                self.features = pd.read_csv(self.fout_features)
            self.expand_features('.full', mapped_ranges[1])
            log.info('writing extended features...')
            self.features.to_csv(features_training, index=False, na_rep="NA")

        # self.filter_features(['snap2aucr'])
        # 'Score(rvET)', 'snap2aucr', 'msa_frequency', 'aromatic', 'score', 'coverage'

        if self.evaluate:
            log.info('evaluating...')
            evaluation = PredEvaluation(type="CV-LOO", model_id=args.model)
            evaluation.create()
            features_cleaned = self.features[~self.features.label.isin(['U', '?'])].reset_index(drop=True)
            features_cleaned = features_cleaned[~features_cleaned.score.isnull()].reset_index(drop=True)
            features_cleaned.to_csv(self.fout_evaluation, index=False, na_rep="NA")
            evaluation.run_single(self.fout_evaluation)
            evaluation.destroy()

    def train_model(self, dataset, model, saveas=None, balanced=True, remove=list(), limit_features=None, fselection=False):
        to_remove = ','.join("%s" % i for i in remove)

        data = converters.load_any_file(dataset)
        data.class_is_last()

        header = Instances.template_instances(data)

        remove_filter = None
        if remove:
            remove_filter = Filter(classname="weka.filters.unsupervised.attribute.Remove", options=["-R", to_remove])

        resample_filter = None
        if balanced:
            resample_filter = Filter(classname="weka.filters.supervised.instance.Resample",
                              options=["-B", "1.0", "-S", "1", "-Z", "200.0"])

        m = Model()
        m.create_model(model)
        cls = m.cls

        if resample_filter:
            resample_filter.inputformat(data)
            data = resample_filter.filter(data)

        if remove_filter:
            fc = FilteredClassifier()
            fc.filter = remove_filter

            if limit_features:
                fs_remove = ','.join("%s" % i for i in limit_features)
                fc_fs = FilteredClassifier()
                fc_fs.filter = Filter(classname="weka.filters.unsupervised.attribute.Remove", options=["-R", fs_remove])
                fc_fs.classifier = cls
                fc.classifier = fc_fs
            else:
                fc.classifier = cls
            #
            # if resample_filter:
            #     model_final = FilteredClassifier()
            #     model_final.filter = resample_filter
            #     model_final.classifier = fc
            # else:
            #     model_final = fc
            model_final = fc
        else:
            # if resample_filter:
            #     model_final = FilteredClassifier()
            #     model_final.filter = resample_filter
            #     model_final.classifier = cls
            # else:
            #     model_final = cls
            model_final = cls

        log.info("Building model...")
        model_final.build_classifier(data)

        m.cls = model_final
        m.header = header
        m.training = data

        if saveas:
            m.save_model(saveas)

        return m


class Prediction:

    base = "/Users/mmiller/Developement/funTRP/R/rtrec"
    predictions = '%s/predictions' % base

    def __init__(self, model, debug=False):
        log.debug('created prediction instance')
        self.model = model
        self.debug = debug

    def set_query(self, query, basepath=None, skip=False):
        self.skip = skip
        if basepath:
            self.query_id = query
            self.query_fa = os.path.join(basepath, query + ".fa")
        else:
            basepath = os.path.dirname(query)
            self.query_fa = os.path.abspath(query)
            self.query_id = os.path.splitext(os.path.basename(self.query_fa))[0]
        self.fout_features = os.path.join(basepath, self.query_id + ".features.csv")

    def run3state(self):

        bash_command_3state = '%s/m3_t.sh %s/selected_features %s/prediction %s/model' % (self.base, self.predictions, self.predictions, self.predictions)
        subprocess.call(bash_command_3state, shell=True)

    def predict3state(self, setid):

        bash_command_3state = '%s/m3_p.sh %s %s/selected_features %s/prediction %s/model' % (self.base, setid, self.predictions, self.predictions, self.predictions)
        print(bash_command_3state)
        subprocess.call(bash_command_3state, shell=True)

    def parse3state(self, suffix='', weight=1):

        predictions = ('T','R','N')

        all = {}

        for p in predictions:
            file = "%s/prediction_%s%s.csv" % (self.predictions, p, suffix)
            with open(file, 'r')as _in:
                csv_in = csv.reader(_in, delimiter=',')
                # inst,actual,predicted,error,prediction,id
                header = next(csv_in)
                for row in csv_in:
                    if len(row) > 0:
                        actual = row[1][-1]
                        predicted = row[2][-1]
                        if row[3] == '+':
                            error = True
                        else:
                            error = False
                        prediction = float(row[4])

                        # if abs(prediction) < 0.9:
                        #     prediction = 0
                        if predicted == "O":
                            # negiated
                            # prediction = prediction * -1
                            prediction = (1 - prediction) - (1 - (1 - prediction))
                        else:
                            prediction = prediction - (1 - prediction)

                        if predicted == "O" and actual in ['N','T']:
                           prediction = prediction * weight

                        # prediction = prediction * 100

                        id = int(row[5])
                        if id not in all:
                            all[id] = {'T':None,'R':None,'N':None}
                        all[id][p] = [predicted, error, prediction]
                        if suffix != '':
                            if actual != "O":
                                all[id]['actual'] = actual
                        elif actual != "O":
                            all[id]['actual'] = actual
                        all[id]['residue'] = row[6]

        out = []
        all_ids_sorted = sorted(all)
        out.append("id,residue,T,R,N,T_score,R_score,N_score,label\n")
        for i in all_ids_sorted:
            T = all[i]['T']
            R = all[i]['R']
            N = all[i]['N']
            if 'actual' not in all[i].keys():
                actual = "O"
            else:
                actual = all[i]['actual']
            residue = all[i]['residue']
            out.append("%i,%s,%s,%s,%s,%f,%f,%f,%s\n" % (i, residue, T[0], R[0], N[0], T[2], R[2], N[2], actual))
        with open('%s/combined%s.csv' % (self.predictions, suffix), 'w') as _out:
            _out.writelines(out)

        return all

    def runM4(self):

        bash_command_M4 = '%s/m4_t.sh %s/combined.csv %s/prediction_M4.csv %s/M4.model' % (self.base, self.predictions, self.predictions, self.predictions)
        subprocess.call(bash_command_M4, shell=True)

    def predictM4(self, setid):

        bash_command_M4 = '%s/m4_p.sh %s %s/combined %s/prediction_M4_%s.csv %s/M4.model' % (self.base, setid, self.predictions, self.predictions, setid, self.predictions)
        print(bash_command_M4)
        subprocess.call(bash_command_M4, shell=True)

    def run3state_single(self, predictions):

        #predictions = ('T', 'R', 'N')
        for p in predictions:
            raw = "%s/selected_features_%svsO.csv" % (self.predictions, p)
            finalPred = "%s/prediction_%s_single.csv" % (self.predictions, p)
            all = []
            all_header = None
            with open(raw, 'r')as raw_in:
                header = next(raw_in)
                entities = raw_in.readlines()
                for idx, e in enumerate(entities):
                    print("%s: %i/%i" % (p, idx,len(entities)))
                    single = "%s/selected_features_%svsO_%i.csv" % (self.predictions, p, idx)
                    remaining = "%s/selected_features_%svsO_%i_rest.csv" % (self.predictions, p, idx)
                    prediction = "%s/prediction_%s_%i.csv" % (self.predictions, p, idx)
                    with open(single, 'w') as single_out:
                        single_out.write(header)
                        single_out.write(e)
                    with open(remaining, 'w') as remaining_out:
                        remaining_out.write(header)
                        remaining_out.writelines("%s" % l for l in entities[0:idx])
                        remaining_out.writelines("%s" % l for l in entities[idx+1:len(entities)])

                    bash_command_3state = '%s/m3_t_single.sh %s %s %s' % (self.base, single, remaining, prediction)
                    subprocess.call(bash_command_3state, shell=True)
                    with open(prediction, 'r') as prediction_out:
                        header_pred = next(prediction_out)
                        if all_header is None:
                            all_header = header_pred
                            all.append(header_pred)
                        pred = next(prediction_out)
                        all.append(pred)
            with open(finalPred, 'w') as finalPred_out:
                finalPred_out.writelines("%s" % l for l in all)

    def runM4_single(self):

        raw = "%s/combined_single.csv" % self.predictions
        finalPred = "%s/prediction_M4.csv" % self.predictions
        all = []
        all_header = None
        with open(raw, 'r')as raw_in:
            header = next(raw_in)
            entities = raw_in.readlines()
            for idx, e in enumerate(entities):
                print("M4: %i/%i" % (idx, len(entities)))
                single = "%s/combined_single_%i.csv" % (self.predictions, idx)
                remaining = "%s/combined_single_%i_rest.csv" % (self.predictions, idx)
                prediction = "%s/prediction_M4_%i.csv" % (self.predictions, idx)
                with open(single, 'w') as single_out:
                    single_out.write(header)
                    single_out.write(e)
                with open(remaining, 'w') as remaining_out:
                    remaining_out.write(header)
                    remaining_out.writelines("%s" % l for l in entities[0:idx])
                    remaining_out.writelines("%s" % l for l in entities[idx + 1:len(entities)])

                bash_command_3state = '%s/m4_t_single.sh %s %s %s' % (self.base, single, remaining, prediction)
                subprocess.call(bash_command_3state, shell=True)
                with open(prediction, 'r') as prediction_out:
                    header_pred = next(prediction_out)
                    if all_header is None:
                        all_header = header_pred
                        all.append(header_pred)
                    pred = next(prediction_out)
                    all.append(pred)
        with open(finalPred, 'w') as finalPred_out:
            finalPred_out.writelines("%s" % l for l in all)

    def evaluate(self, prediction_file):

        table = {}
        instance_counter = 0

        with open(prediction_file, 'r')as _in:
            csv_in = csv.reader(_in, delimiter=',')
            # inst,actual,predicted,error,prediction,id
            header = next(csv_in)
            for row in csv_in:
                if len(row) > 0:
                    actual = row[1][-1]
                    actual_class = row[1][0]
                    predicted = row[2][-1]
                    predicted_class = row[2][0]
                    if row[3] == '+':
                        error = True
                    else:
                        error = False
                    prediction = float(row[4])
                    id = int(row[5])

                    if not actual in table.keys():
                        table[actual] = {}

                    if not predicted in table[actual].keys():
                        table[actual][predicted] = 0

                    table[actual][predicted] += 1
                    instance_counter += 1

        labels = list(table.keys())
        true_pos = table[labels[0]][labels[0]]
        true_neg = table[labels[1]][labels[1]]

        accuracy = (true_pos + true_neg) / float(instance_counter)

        return table, accuracy

    def human_enzymes(self, mapping):
        all = []
        with open(mapping, 'r') as mapping_in:
            #Entry	Entry name	EC number
            header = next(mapping_in)
            for row in mapping_in.readlines():
                has_ec = False
                row_ = row.strip().split('\t')
                if len(row_) == 3:
                    has_ec = True
                if has_ec:
                    all.append(row_)

        pmd_classes = ['mild','moderate','neutral','severe']
        pmd_ids = {'mild': [],'moderate': [],'neutral': [],'severe': []}
        mapped_ids = {'mild': [], 'moderate': [], 'neutral': [], 'severe': []}
        for pmd in pmd_classes:
            input = '/Users/mmiller/Downloads/PMD_for_Max/pmd_%s_human.ids' % pmd
            pmd_ids[pmd] = [line.rstrip() for line in open(input)]
            print('%s: %i' % (pmd, len(pmd_ids[pmd])))

        for enzyme in all:
            entry = enzyme[0]
            name = enzyme[1]
            ec = enzyme[2]
            for pmd in pmd_classes:
                if name in pmd_ids[pmd]:
                    mapped_ids[pmd].append(name)

        uniq_ids = set()
        print("-- Mapping --")
        for pmd in pmd_classes:
            print('%s: %i' % (pmd, len(mapped_ids[pmd])))
            uniq_ids.update(mapped_ids[pmd])
        print('unique: %i' % len(uniq_ids))
        output = '/Users/mmiller/Downloads/PMD_for_Max/unique.ids'
        with open(output, 'w') as out_:
            out_.writelines("%s," % i for i in uniq_ids)


        return mapped_ids, uniq_ids

    def run(self):

        if self.skip:
            log.info("skipping feature extraction...")
            self.features = pd.read_csv(self.fout_features)
        else:
            self.features = extract_features(self.query_id, self.query_fa, None)
            log.info('writing features...')
            self.features.to_csv(self.fout_features, index=False, na_rep="NA")

        # DEPRECATED
        # methods.csv2arff(query_fasta)
        # methods.single_models(query_fasta)
        # methods.parse3state(query_fasta, weight=100)
        # methods.csv2arff_combined(query_fasta)
        # methods.model4(query_fasta)

    def predict_model(self, dataset, saveAs=None, addcolumns=list()):
        out_columns_added = ','.join("%s" % i for i in addcolumns)

        data = converters.load_any_file(dataset)
        data.class_is_last()

        data_header = Instances.template_instances(data)
        data_labels = data_header.attribute_by_name('label').values

        model_header = Instances.template_instances(self.model.header)
        model_labels = model_header.attribute_by_name('label').values

        # print(model_labels)
        # print(data_labels)

        pout = PredictionOutput(classname="weka.classifiers.evaluation.output.prediction.CSV")
        if out_columns_added:
            pout.options = ["-p", out_columns_added]

        csv_header = "inst,actual,predicted,error,prediction,prediction_T,prediction_N,class_labels,class_predictions"
        if model_labels.count('R') != 0:
            csv_header += ",prediction_R"
        for a in addcolumns:
            csv_header = csv_header + ',' + data_header.attribute(a - 1).name
        predictions = []
        for index, instance in enumerate(data):
            pred = self.model.cls.classify_instance(instance)
            dist = self.model.cls.distribution_for_instance(instance)
            actual_label = instance.get_string_value(instance.class_index)
            # predicted_label = instance.class_attribute.value(int(pred)),
            predicted_label = model_labels[int(pred)]
            csv_row = "%d,%i:%s,%i:%s,%s,%s,%s,%s,%s,%s" %\
                (index + 1,
                 instance.get_value(instance.class_index),
                 actual_label,
                 int(pred),
                 predicted_label,
                 # "+" if pred != instance.get_value(instance.class_index) else "",
                 "+" if predicted_label != actual_label else "",
                 #str(dist.tolist())
                 dist.tolist()[int(pred)],
                 dist.tolist()[model_labels.index('T')],
                 dist.tolist()[model_labels.index('N')],
                 str(model_labels).replace(",", ";"),
                 str(dist.tolist()).replace(",", ";"),
                 )
            if model_labels.count('R') != 0:
                csv_row += ',%s' % dist.tolist()[model_labels.index('R')]
            for a in addcolumns:
                csv_row = csv_row + ',' + str(instance.get_string_value(a-1))
            res = (index, int(pred), model_labels, dist, instance, csv_row)
            predictions.append(res)
        csv = csv_header
        for p in predictions:
            csv = csv + "\n" + p[-1]

        # evaluate
        # train = converters.load_any_file(self.query_id)
        # train.class_is_last()
        # evaluation = Evaluation(train)
        # evl = evaluation.test_model(self.model.cls, train)
        # print(evl)
        # print(evaluation.summary())

        if saveAs:
            with open(saveAs, 'w') as fout:
                #fout.write(pout.buffer_content())
                fout.write(csv)

        return predictions, csv


class FeatureSelection:

    def __init__(self, dataset, csv=False, debug=False, remove=list()):
        log.debug('created feature selection instance')
        self.dataset = dataset
        self.attsel = None
        self.debug = debug

        if csv:
            self.dataset = converters.load_any_file(dataset)
            self.dataset.class_is_last()

        if remove:
            self.dataset = self.remove_attributes(remove, self.dataset)

    def remove_attributes(self, remove, dataset):
        to_remove = ','.join("%s" % i for i in remove)
        remove = Filter(classname="weka.filters.unsupervised.attribute.Remove", options=["-R", to_remove])
        remove.inputformat(dataset)
        dataset = remove.filter(dataset)
        return dataset
        # dataset.class_is_last()

    def evaluate(self, dataset_csv, start, end, outfile, remove=list()):
        header=None
        avg_scores=list()
        threshold = 0.03
        with open(outfile, 'w') as fout:
            selected_features = set()
            if not header:
                header = []
                tmp = converters.load_any_file(dataset_csv)
                if remove:
                    tmp = self.remove_attributes(remove, tmp)
                for a in tmp.attributes():
                    header.append(a.name)
                    avg_scores.append((0,0))
                fout.write(','.join("%s" % i for i in header) + '\n')
            for idx in range(start, end):
                dataset = converters.load_any_file(dataset_csv)
                if remove:
                    dataset = self.remove_attributes(remove, dataset)
                dataset.class_is_last()
                dataset.delete(idx)
                self.dataset = dataset
                self.relief()
                selected = self.attsel.selected_attributes
                fout.write(','.join("%s" % i for i in selected) + '\n')
                scores = self.attsel.ranked_attributes
                for rank in range(0, len(scores)):
                    idx_feature = int(scores[rank][0])
                    score = scores[rank][1]
                    # print(idx_feature, score)
                    if score >= threshold:
                        selected_features.add(idx_feature)
                        avg_scores[idx_feature] = (avg_scores[idx_feature][0] + score, avg_scores[idx_feature][1] + 1)
                        # print(selected_features)
                #print(selected)
                #print(self.attsel.results_string)
        return (list(selected_features), [header[idx] for idx in selected_features], [avg_scores[idx][0]/avg_scores[idx][1] for idx in selected_features])

    def relief(self):
        search = ASSearch(classname="weka.attributeSelection.Ranker",
                          options=["-T", "-1.7976931348623157E308", "-N", "-1"])
        evaluator = ASEvaluation(classname="weka.attributeSelection.ReliefFAttributeEval",
                                 options=["-M", "-1", "-D", "1", "-K", "10"])
        self.attsel = AttributeSelection()
        self.attsel.search(search)
        self.attsel.evaluator(evaluator)
        self.attsel.select_attributes(self.dataset)

    def cfs(self):
        search = ASSearch(classname="weka.attributeSelection.GreedyStepwise",
                          options=["-T", "-1.7976931348623157E308", "-N", "-1", "-num-slots", "1"])
        evaluator = ASEvaluation(classname="weka.attributeSelection.CfsSubsetEval",
                                 options=["-P", "-1", "-E", "1"])
        self.attsel = AttributeSelection()
        self.attsel.search(search)
        self.attsel.evaluator(evaluator)
        self.attsel.select_attributes(self.dataset)
        # print(self.attsel.selected_attributes)
        # print(self.attsel.results_string)
        # print(self.dataset)


class PredEvaluation:

    def __init__(self, model_id='RT', type="CV-10", debug=False):
        log.debug('created evaluation instance')
        self.model_id = model_id
        self.type = type
        self.debug = debug

    def run_single(self, dataset, outfile=None, remove=list(), limit_features=None, fselection=False):
        """

        :param dataset:
        :param outfile:
        :param remove:
        :param fselection:
        :return:
        """
        reduced_dataset = converters.load_any_file(dataset)
        if remove:
            reduced_dataset.class_is_last()
            to_remove = ','.join("%s" % i for i in remove)
            remove_filter = Filter(classname="weka.filters.unsupervised.attribute.Remove", options=["-R", to_remove])
            remove_filter.inputformat(reduced_dataset)
            reduced_dataset = remove_filter.filter(reduced_dataset)
            log.info("Features remaining after removing [%s]: %s" % (to_remove, "Instances.template_instances(reduced_dataset)"))
        if limit_features:
            to_remove2 = ','.join("%s" % i for i in limit_features)
            remove_filter = Filter(classname="weka.filters.unsupervised.attribute.Remove", options=["-R", to_remove2])
            remove_filter.inputformat(reduced_dataset)
            reduced_dataset = remove_filter.filter(reduced_dataset)
            log.info("Features remaining after removing [%s]: %s" % (to_remove2, "Instances.template_instances(reduced_dataset)"))

        to_remove = ','.join("%s" % i for i in remove)

        data = converters.load_any_file(dataset)
        data.class_is_last()

        if self.type.split("-")[1] == "LOO":
            cv_fold = data.num_instances
        else:
            cv_fold = int(self.type.split("-")[1])

        if remove:
            remove_filter = Filter(classname="weka.filters.unsupervised.attribute.Remove", options=["-R", to_remove])
        else:
            remove_filter = Filter(classname="weka.filters.unsupervised.attribute.Remove")
        resample = Filter(classname="weka.filters.supervised.instance.Resample",
                          options=["-B", "1.0", "-S", "1", "-Z", "1000.0"])

        # if fselection:
        #     to_retain = list()
        #     fs = FeatureSelection(data, remove=remove)
        #     fs.cfs()
        #     # fs.attsel = AttributeSelection(fs.attsel)
        #     print(fs.attsel.results_string)
        #     to_retain = fs.attsel.selected_attributes
        #     fs_remove = list()
        #     for i in range(1, fs.dataset.class_index):
        #         if i not in to_retain:
        #             fs_remove.append(i)
        #     to_remove = ','.join("%s" % i for i in fs_remove)
        #     log.info(to_retain)
        #     log.info(to_remove)
        #     fselection_remove_filter = Filter(classname="weka.filters.unsupervised.attribute.Remove", options=["-R", to_remove])
        # else:
        #     fselection_remove_filter = Filter(classname="weka.filters.unsupervised.attribute.Remove")

        m = Model()
        m.create_model(self.model_id)
        cls = m.cls

        fc3 = Classifier(classname="weka.classifiers.meta.AttributeSelectedClassifier")
        # aseval = ASEvaluation(classname="weka.attributeSelection.CfsSubsetEval", options=["-P", "-1", "-E", "1"])
        aseval = ASEvaluation(classname="weka.attributeSelection.ReliefFAttributeEval", options=["-M", "-1", "-D", "1", "-K", "10"])
        # assearch = ASSearch(classname="weka.attributeSelection.GreedyStepwise",
        #                     options=["-T", "-1.7976931348623157E308", "-N", "-1", "-num-slots", "1"])
        # assearch = ASSearch(classname="weka.attributeSelection.BestFirst",
        #                       options=["-D", "1", "-N", "5"])
        assearch = ASSearch(classname="weka.attributeSelection.Ranker",
                            options=["-T", "-1.7976931348623157E308", "-N", "15"])

        fc3.set_property("classifier", cls.jobject)
        fc3.set_property("evaluator", aseval.jobject)
        fc3.set_property("search", assearch.jobject)

        fc2 = FilteredClassifier()
        fc2.filter = remove_filter

        if limit_features:
            fs_remove = ','.join("%s" % i for i in limit_features)
            fc_fs = FilteredClassifier()
            fc_fs.filter = Filter(classname="weka.filters.unsupervised.attribute.Remove", options=["-R", fs_remove])
            fc_fs.classifier = cls
            fc2.classifier = fc_fs
        elif fselection:
            fc2.classifier = fc3
        else:
            fc2.classifier = cls

        fc1 = FilteredClassifier()
        fc1.filter = resample
        fc1.classifier = fc2

        log.info("Building model...")
        fc1.build_classifier(data)

        pout = PredictionOutput(classname="weka.classifiers.evaluation.output.prediction.CSV", options=["-p", "1,3"])

        log.info("Cross-Validation: %i [%i]" % (cv_fold, data.num_instances))
        evl = Evaluation(data)
        evl.crossvalidate_model(fc1, data, cv_fold, Random(1), pout)

        print("\n=== Classifier: %s - %f ===" % ((cls.classname).split('.')[-1], evl.percent_correct))
        print(evl.summary())
        print(evl.class_details())
        print(evl.matrix())

        predictions_out = pout.buffer_content()
        predictions_out_list = predictions_out.split("\n")
        positions = list()
        for line in predictions_out_list[1:len(predictions_out_list)-1]:
            positions.append(line.split(',')[5])

        if outfile:
            with open(outfile, 'w') as fout:
                fout.write(predictions_out)

        return evl, positions, data

    def parse_eval(self, pred_csv):
        predictions = {}
        with open(pred_csv, 'r')as _in:
            csv_in = csv.reader(_in, delimiter=',')
            # inst,actual,predicted,error,prediction,id,residue
            header = next(csv_in)
            for row in csv_in:
                if len(row) > 0:
                    instance = row[0]
                    label = row[-1]
                    actual = row[1][-1]
                    predicted = row[2][-1]
                    if row[3] == '+':
                        error = True
                    else:
                        error = False
                    prediction = float(row[4])
                    id = row[5]
                    residue = row[6]
                    predictions [id] = (instance, actual, predicted, error, prediction, id, residue)
        return predictions


    def parse3state(self, dataset, suffix='_predictions.csv', weight_R=1, weight_T=1, weight_N=1):
        filename = os.path.splitext(os.path.basename(dataset))[0]
        basepath = os.path.dirname(dataset)
        prefix = os.path.join(basepath, filename)

        predictions = ('T','R','N')
        predictions2 = ('RT', 'RN', 'TN')

        all = {}

        evaluations_in = os.path.join(basepath, "evaluation.csv")
        with open(evaluations_in, 'r')as _in:
            csv_in = csv.reader(_in, delimiter=',')
            # inst,actual,predicted,error,prediction,id,residue
            header = next(csv_in)
            for row in csv_in:
                if len(row) > 0:
                    id = row[0]
                    label = row[-1]
                    all[id] = {'label': label, 'residue': None, 'T': None, 'R': None, 'N': None, 'RT': None, 'RN': None, 'TN': None}

        for p in predictions:
            file = "%s_%svsO%s" % (prefix, p, suffix)
            with open(file, 'r')as _in:
                csv_in = csv.reader(_in, delimiter=',')
                # inst,actual,predicted,error,prediction,id,residue
                header = next(csv_in)
                for row in csv_in:
                    if len(row) > 0:
                        actual = row[1][-1]
                        predicted = row[2][-1]
                        if row[3] == '+':
                            error = True
                        else:
                            error = False
                        prediction = float(row[4])

                        if predicted != p:
                            prediction = (1 - prediction) - (1 - (1 - prediction))
                        else:
                            prediction = prediction - (1 - prediction)

                        if predicted == "O" and p in ['R']:
                           prediction = prediction * weight_R
                        elif predicted == "O" and p in ['T']:
                           prediction = prediction * weight_T
                        elif predicted == "O" and p in ['N']:
                           prediction = prediction * weight_N

                        id = row[5]
                        all[id][p] = [predicted, error, prediction]
                        all[id]['actual'] = actual
                        all[id]['residue'] = row[6]

        for p in predictions2:
            file = "%s_%svs%s%s" % (prefix, p[0], p[1], suffix)
            with open(file, 'r')as _in:
                csv_in = csv.reader(_in, delimiter=',')
                # inst,actual,predicted,error,prediction,id,residue
                header = next(csv_in)
                for row in csv_in:
                    if len(row) > 0:
                        actual = row[1][-1]
                        predicted = row[2][-1]
                        if row[3] == '+':
                            error = True
                        else:
                            error = False
                        prediction = float(row[4])

                        if predicted != p:
                            prediction = (1 - prediction) - (1 - (1 - prediction))
                        else:
                            prediction = prediction - (1 - prediction)

                        id = row[5]
                        all[id][p] = [predicted, error, prediction]
                        all[id]['actual'] = actual
                        all[id]['residue'] = row[6]

        out = []
        all_ids_sorted = sorted(all)
        out.append("id,residue,T,R,N,RvsT,RvsN,TvsN,T_score,R_score,N_score,RvsT_score,RvsN_score,TvsN_score,label\n")
        for i in all_ids_sorted:
            T = all[i]['T']
            R = all[i]['R']
            N = all[i]['N']
            RT = all[i]['RT']
            RN = all[i]['RN']
            TN = all[i]['TN']
            # if 'actual' not in all[i].keys():
            #     actual = "O"
            # else:
            #     actual = all[i]['actual']
            residue = all[i]['residue']
            out.append("%s,%s,%s,%s,%s,%f,%f,%f,%s,%s,%s,%f,%f,%f,%s\n" % (i, residue, T[0], R[0], N[0], T[2], R[2], N[2], RT[0], RN[0], TN[0], RT[2], RN[2], TN[2], all[i]['label']))
        with open('%s.combined%s' % (prefix, suffix), 'w') as _out:
            _out.writelines(out)


class Model:

    def __init__(self):
        self.model_id = None
        self.cls = None
        self.header = None
        self.training = None

    def load_model(self, model_file):
        objects = serialization.read_all(model_file)
        self.cls = Classifier(jobject=objects[0])
        self.header = Instances(jobject=objects[1])
        self.training = Instances(jobject=objects[2])

    def save_model(self, model_file):
        serialization.write_all(model_file, [self.cls, self.header, self.training])

    def create_model(self, model_id):
        self.model_id = model_id
        cls = None

        if self.model_id == "SVM":
            cls = KernelClassifier(classname="weka.classifiers.functions.SMO",
                                   options=["-C", "1.0", "-L", "0.001", "-P", "1.0E-12", "-N", "0", "-V", "-1", "-W",
                                            "1", "-calibrator",
                                            "weka.classifiers.functions.Logistic -R 1.0E-8 -M -1 -num-decimal-places 4"])
            kernel = Kernel(classname="weka.classifiers.functions.supportVector.PolyKernel",
                            options=["-E", "1.0", "-C", "250007"])
            cls.kernel = kernel
        elif self.model_id == "RF":
            cls = Classifier(classname="weka.classifiers.trees.RandomForest",
                             options=["-P", "100", "-I", "100", "-num-slots", "1", "-K", "0", "-M", "1.0", "-V",
                                      "0.001", "-S", "1"])
        elif self.model_id == "RT":
            cls = Classifier(classname="weka.classifiers.trees.RandomTree",
                             options=["-K", "7", "-M", "1.0", "-V", "0.001", "-S", "1"])
        elif self.model_id == "LOG":
            cls = Classifier(classname="weka.classifiers.functions.Logistic",
                             options=["-R", "1.0E-8", "-M", "-1", "-num-decimal-places", "4"])
        else:
            log.error('Model %s not known!' % self.model_id)

        self.cls = cls
        return cls


def get_argument_parser():
    """Argument parser if module is called from command line, see main method."""

    parser = argparse.ArgumentParser(description='Rheostat Predictor (dev)')
    parser.add_argument('query', metavar='query.fasta',
                        help='query sequence file in fasta format')
    parser.add_argument('dataset', metavar='dataset.csv',
                        help='parsed experimental deep sequencing dataset in csv format')
    parser.add_argument('-s', '--stats', metavar='output_stats.csv',
                        help='Writes statistics for the parsed dataset to the given output file')
    return parser


if __name__ == "__main__":
    args = get_argument_parser().parse_args()
    args.query = os.path.abspath(args.query)
    args.dataset = os.path.abspath(args.dataset)
