import argparse
import subprocess
import csv
import re
import shutil
import os
from Bio import AlignIO, SeqIO
from bblsuite.helper import logger, config


class Conservation:
    """Class to calculate conservation scores of MSAs"""

    log = logger.Logger.get_logger()

    def __init__(self, query_file, msa_file, msa_format_):
        self.config = config.Config()
        self.query = None
        self.consMethod_index = {}
        self.query_file = os.path.abspath(query_file)
        self.msa_file = os.path.abspath(msa_file)
        self.msa_format = msa_format_
        self.wd = os.path.dirname(self.query_file)
        self.log.debug("Changing working directory to %s" % self.wd)
        os.chdir(self.wd)
        self.read_query_sequence()

        self.query_subjects = self.wd + '/' + self.query.id + '.query_subjects'
        self.blast_out = self.wd + '/' + self.query.id + '.blast'
        self.msa_subjects_out = self.wd + '/' + self.query.id + '.msa_subjects'
        self.query_subsequence = self.wd + '/' + self.query.id + '.query_subsequence'
        self.msa_subjects_combined_out = self.wd + '/' + self.query.id + '.msa_subjects_combined'
        self.msa_combined_out = self.wd + '/' + self.query.id + '.msa_combined'
        self.msa_combined_out_dashonly = self.wd + '/' + self.query.id + '.msa_combined_dashonly'
        self.msa_converted_out = self.wd + '/' + self.query.id + '.msa_converted'

        conservation_out = self.wd + '/' + self.query.id + '.conservation'
        labels_out = self.wd + '/' + self.query.id + '.labels'

        self.init_cons_method_index()

    def init_cons_method_index(self):
        """Add here any new function to calculate conservation scores here to be registered."""

        self.add_cons_method_function(self.rate4site, "rate4site")
        self.add_cons_method_function(self.conscalc, "consCalc")
        self.add_cons_method_function(self.evolutionary_trace_server, "wetc (Evolutionary Trace Server)")

    def add_cons_method_function(self, func, description):
        """Adds new conservation calculation method"""

        method_id = len(self.consMethod_index) + 1
        self.consMethod_index[method_id] = [func, description]

    def get_cons_method_function(self, method_id):
        """Returns conservation calculation method from list: consMethod_index"""

        return self.consMethod_index.get(method_id)[0]

    def get_cons_method_description(self, method_id):
        """Returns conservation calculation method description from list: consMethod_index"""

        return self.consMethod_index.get(method_id)[1]

    def read_query_sequence(self):
        """Read query fasta file"""

        self.query = SeqIO.read(self.query_file, "fasta")
        self.log.info("Got query sequence id:%s, length:%i" % (self.query.id, len(self.query.seq)))

    def blast_selected_subjects(self):
        """Create new MSA"""

        msa_subset = self.wd + '/' + 'msa_subset.fasta'
        bash_command_blast = '%s -query %s -subject %s -out %s -max_target_seqs 10000 -outfmt "6 sseqid"' % (self.config.get_property('Software.alpha', 'blastp'), self.query_file, False, msa_subset)

    def blast_query_against_msa(self):
        """Extract query subjects from MSA file and run blast

        of query against those subjects to yield best matching subject"""

        bash_command_sed_sequences = 'sed -e "/^[^>]/ s/[\.-]//g" %s > %s' % (self.msa_file, self.wd + '/' + self.query_subjects)
        subprocess.call(bash_command_sed_sequences, shell=True)

        bash_command_blast = '%s -query %s -subject %s -out %s -max_target_seqs 10 -max_hsps 1 -outfmt "6 std qseq sseq"' % (self.config.get_property('Software.alpha', 'blastp'), self.query_file, self.query_subjects, self.blast_out)
        subprocess.call(bash_command_blast, shell=True)

        query_msaseq = ''
        query_start = -1
        query_end = -1
        subject_list = []
        with open(self.blast_out, 'r') as tsv_in:
            tsv_in = csv.reader(tsv_in, delimiter='\t')
            for row in tsv_in:
                # qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qseq sseq
                qseqid = row[0]
                sseqid = row[1]
                pident = row[2]
                qstart = int(row[6])
                qend = int(row[7])
                evalue = row[10]
                bitscore = row[11]
                qseq = row[12]
                sseq = row[13]

                if (qend - qstart) > (query_end - query_start):
                    query_start = qstart
                    query_end = qend

                subject_list += [sseqid]

        subjectseq_list = []
        for msa in AlignIO.parse(self.msa_file, self.msa_format):
            for ali in msa:
                if ali.id in subject_list:
                    subjectseq_list += [ali.seq]
                else:
                    self.log.warn('id [%s] not found in list of subjects!' % ali.id)

        with open(self.msa_subjects_out, 'w') as out:
            for idx, val in enumerate(subject_list):
                out.write(">%s\n%s\n" % (str(subject_list[idx]), str(subjectseq_list[idx])))

        with open(self.query_subsequence, 'w') as query_sub:
            subseq = self.query.seq[query_start - 1:query_end]
            query_sub.write(">%s\n%s" % (self.query.id, subseq))

        bash_command_muscle = '%s -profile -in1 %s -in2 %s -out %s' % (self.config.get_property('Software.alpha', 'muscle'), self.msa_subjects_out, self.query_subsequence, self.msa_subjects_combined_out)
        subprocess.call(bash_command_muscle, shell=True)

        for msa_combined in AlignIO.parse(self.msa_subjects_combined_out, self.msa_format):
            for ali in msa_combined:
                if ali.id == self.query.id:
                    query_msaseq = ali.seq

        shutil.copyfile(self.msa_file, self.msa_combined_out)
        with open(self.msa_combined_out, 'a') as msa_append:
            msa_append.write(">%s\n%s\n" % (self.query.id, query_msaseq))

        bash_command_sed_msadashonly = 'sed -e "/^[^>]/ s/\./-/g" %s > %s' % (self.msa_combined_out, self.msa_combined_out_dashonly)
        subprocess.call(bash_command_sed_msadashonly, shell=True)

        bash_command_emboss = '%s %s %s -sformat %s -osformat %s' % (self.config.get_property('Software.alpha', 'EMBOSS'), self.msa_combined_out, self.msa_converted_out, 'fasta', 'msf')
        subprocess.call(bash_command_emboss, shell=True)

    def get_conservtation(self, method_id):
        """Calculate conservation of residue using a user selectable method"""

        query_start = -1
        query_end = -1
        self.blast_query_against_msa()
        with open(self.blast_out, 'r') as tsv_in:
            tsv_in = csv.reader(tsv_in, delimiter='\t')

            for row in tsv_in:
                qstart = int(row[6])
                qend = int(row[7])

                if (qend - qstart) > (query_end - query_start):
                    query_start = qstart
                    query_end = qend

        conservation_fct = self.get_cons_method_function(method_id)
        conservation_scores = conservation_fct()

        return conservation_scores

    def rate4site(self, binary=None):
        """Calculate conservation scores using rate4site"""

        if not binary:
            binary = self.config.get_property('Software.alpha', 'rate4site')

        self.log.info("rate4site: calculating conservation...")

        res = {}

        rate4site_out = self.wd + '/' + self.query.id + '.rate4site'
        bash_command_rate4site = '%s -a %s -o %s -s %s' % (binary, self.query.id, rate4site_out, self.msa_combined_out_dashonly)
        subprocess.call(bash_command_rate4site, shell=True)

        self.log.info("rate4site: done")

        # parse the output
        with open(rate4site_out, 'r') as res:
            for line in res:
                line = line.strip()
                # TODO: check if b'' needed
                if line.startswith('#') or line.__len__() == 0:
                    continue
                else:
                    row = re.split('\s+', line)
                    res[int(row[0])] = row
        return res

    def conscalc(self):
        """Calculate conservation scores using conscalc"""

        self.log.info("conscalc: calculating conservation...")

        res = {}

        cons_calc_out = self.wd + '/' + self.query.id + '.consCalc'
        bash_command_cons_calc = '%s -a %s -o %s %s' % (self.config.get_property('Software.alpha', 'conscalc'), self.query.id, cons_calc_out, self.msa_combined_out_dashonly)
        subprocess.call(bash_command_cons_calc, shell=True)

        self.log.info("conscalc: done")

        # parsing
        idx = 1
        with open(cons_calc_out, 'r') as res:
            for line in res:
                line = line.strip()
                # TODO: check if b'' needed
                if line.startswith("#") or line.__len__() == 0:
                    continue
                else:
                    row = re.split('\s+', line)
                    res[idx] = row
                    idx += 1
        return res

    def evolutionary_trace_server(self):
        """Calculate conservation scores using wetc (Evolutionary Trace Server)"""

        self.log.info("wetc: calculating conservation...")

        res = {}

        wetc_out = self.wd + '/' + self.query.id + '.ranks_sorted'
        bash_command_wetc = '%s -p %s -x %s -o %s' % (self.config.get_property('Software.alpha', 'wetc'), self.msa_converted_out, self.query.id, self.query.id)
        subprocess.call(bash_command_wetc, shell=True)

        self.log.info("wetc: done")

        # parsing
        with open(wetc_out, 'r') as res:
            for line in res:
                line = line.strip()
                # TODO: check if b'' needed
                if line.startswith("%") or line.__len__() == 0:
                    continue
                else:
                    row = re.split('\s+', line)
                    res[int(row[1])] = row
        return res


def get_argument_parser():
    """Argument parser if module is called from command line, see main method."""

    parser = argparse.ArgumentParser(description='Calculate Conservation')

    parser.add_argument('query', metavar='query', type=str, nargs='?',
                        help='query sequence file (fasta)')
    parser.add_argument('msa', metavar='msa', type=str, nargs='?',
                        help='source MSA file')
    parser.add_argument('source', metavar='msa_format', type=str, nargs='?',
                        help='source MSA format')
    parser.add_argument('expdata', metavar='dataset', type=str, nargs='?',
                        help='experimental deepseq data file')
    return parser


if __name__ == "__main__":
    ArgParser = get_argument_parser()


def label_toggles_based_on_absolute_counts(position_analysis):
    """DEPRECATED. Label toggle and rheostat based on absolut ratios"""

    toggle_severe = .61
    toggle_neutral_wt = .24
    toggle_neutral_nowt = .16

    wt = position_analysis["wt_score"]
    nas = position_analysis["na_substitutions"]
    variant_count = 20 - int(nas)
    ne = position_analysis["neutral"]
    nne = position_analysis["non-neutral"]
    im = position_analysis["intermediate"]

    toggle = "NA"

    if variant_count == 0:
        return

    non_neutral = float(nne) / variant_count
    neutral = float(ne) / variant_count

    if variant_count >= 13:
        if wt:
            if (non_neutral >= toggle_severe) and (neutral <= toggle_neutral_wt):
                toggle = 1
            else:
                toggle = 0
        else:
            if (non_neutral >= toggle_severe) and (neutral <= toggle_neutral_nowt):
                toggle = 1
            else:
                toggle = 0
    else:
        if int(ne) > 4:
            toggle = 0

    return toggle


def label_toggles_based_on_absolute_counts_refined(position_analysis):
    """DEPRECATED. Label toggle and rheostat based on absolut ratios; refined version"""

    toggle_severe = .61
    toggle_neutral_wt = .24
    toggle_neutral_nowt = .16

    wt = position_analysis["wt_score"]
    nas = position_analysis["na_substitutions"]
    variant_count = 20 - int(nas)
    ne = position_analysis["neutral"]
    nne = position_analysis["non-neutral"]
    im = position_analysis["intermediate"]

    toggle = "NA"

    if variant_count == 0:
        return

    non_neutral = float(nne) / variant_count
    neutral = float(ne) / variant_count

    if (nne <= 2) and (neutral >= 0.9):
        return -1

    if variant_count >= 13:
        if wt:
            if (non_neutral >= toggle_severe) and (neutral <= toggle_neutral_wt):
                toggle = 1
            else:
                toggle = 0
        else:
            if (non_neutral >= toggle_severe) and (neutral <= toggle_neutral_nowt):
                toggle = 1
            else:
                toggle = 0
        return toggle

    if variant_count < 13:
        if wt:
            if (non_neutral >= toggle_severe) and (neutral <= toggle_neutral_wt):
                toggle = 1
            else:
                toggle = 0
        else:
            if (non_neutral >= toggle_severe) and (neutral <= toggle_neutral_nowt):
                toggle = 1
            else:
                toggle = 0
        return toggle

    return "NA"


# def tmp(query_sequence, query_analysis, query_conservation, conservation_threshold):
#     for query_idx, residue in enumerate(query_sequence):
#
#         analysis = query_analysis[query_idx]
#         conservation = query_conservation[query_idx]
#
#         if query_idx < query_start - 1 or query_idx + 1 > query_end:
#             self.logger.debug("Skipping residue %s" % str(query_idx + 1))
#             continue
#
#         if conservation < 0:
#             self.logger.debug("No conservation value at position %i" % query_idx)
#             continue
#
#         conserved = conservation > conservation_threshold
#         label = "NA"
