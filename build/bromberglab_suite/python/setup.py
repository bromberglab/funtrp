#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name='bblsuite',
    version='0.1.dev0',
    description='BrombergLab Python Suite',
    long_description='Collection of software to decode the genomic blueprint of life (c) BrombergLab',
    url='https://bitbucket.org/bromberglab/bromberglab_suite.git',
    author='Yannick Mahlich, Maximilian Miller',
    author_email='ymahlich@bromberglab.org, mmiller@gmail.com',
    license='MIT',
    platforms='various',
    packages=find_packages(),
    install_requires=[
        'pandas', 'rpy2', 'biopython', ' mysqlclient', 'paramiko', 'pexpect'
    ],
)
