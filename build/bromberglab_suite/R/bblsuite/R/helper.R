find_peaks <- function (x, m = 3){
  shape <- diff(sign(diff(x, na.pad = FALSE)))
  pks <- sapply(which(shape < 0), FUN = function(i){
    z <- i - m + 1
    z <- ifelse(z > 0, z, 1)
    w <- i + m + 1
    w <- ifelse(w < length(x), w, length(x))
    if(all(x[c(z : i, (i + 2) : w)] <= x[i + 1])) return(i + 1) else return(numeric(0))
  })
  pks <- unlist(pks)
  pks
}

aucr <- function(scores, auc_separator = NULL, plot = FALSE, plot_id = 'NA')
{
  if (length(na.omit(scores)) < 2)
    return(NA)

  d <- density(scores,na.rm = T)
  if (is.null(auc_separator))
  {
    auc_separator <- median(d$x)
  }
  if (length(which(d$x > auc_separator)) < 2 | length(which(d$x < auc_separator)) < 2)
  {
    return(0)
  }
  auc_right <- flux::auc(d$x[which(d$x > auc_separator)],d$y[which(d$x > auc_separator)])
  auc_left <- flux::auc(d$x[which(d$x <= auc_separator)],d$y[which(d$x <= auc_separator)])
  aucr <- auc_right/auc_left
  if (plot)
  {
    aux_right_x <- d$x[which(d$x > auc_separator)]
    aux_right_y <- d$y[which(d$x > auc_separator)]
    aux_left_x <- d$x[which(d$x <= auc_separator)]
    aux_left_y <- d$y[which(d$x <= auc_separator)]
    p <- plotly::plot_ly(x = ~aux_right_x, y = ~aux_right_y, type = 'scatter', mode = 'lines', name = paste0("auc_right: ",format(auc_right, digits=2, nsmall=2)), fill = 'tozeroy') %>%
      plotly::add_trace(x = ~aux_left_x, y = ~aux_left_y, name = paste0("auc_left: ",format(auc_left, digits=2, nsmall=2)), fill = 'tozeroy') %>%
      plotly::add_trace(x = c(median(d$x), median(d$x)), y= c(min(d$y), max(d$y)), mode = "lines", name = 'auc_separator', fill = NULL, line = list(dash = "dash", width = 1), color = I("grey")) %>%
      plotly::layout(yaxis = list(title = "density"), xaxis = list(title = "measured experimental value"), title = paste0("Kernel density plot (",plot_id,"):, aucr: ",format(aucr, digits=2, nsmall=2)))
    return(list(plot=p, aucr=aucr))
  }
  else
  {
    return(aucr)
  }
}

normalize_fixed_range_mapping <- function(values, minv, maxv, minnorm, maxnorm)
{
  threshold_mapping <- c(mean(c(maxv,minv)),mean(c(maxnorm,minnorm)))
  sapply(values, function(x)
  {
    if(!is.na(x))
    {
      if (x <= threshold_mapping[1])
      {
        ((x - minv)/(threshold_mapping[1] - minv)) * (threshold_mapping[2] - minnorm) + minnorm
      }
      else if (x > threshold_mapping[1])
      {
        ((x - threshold_mapping[1])/(maxv - threshold_mapping[1])) * (maxnorm - threshold_mapping[2]) + threshold_mapping[2]
      }
    }
    else x
  })
}

normalize_two_sided <- function(wt,neutral_lower_percent, neutral_upper_percent, dataRange, values, percentage = T)
{
  minlim <- min(dataRange, na.rm=TRUE)
  maxlim <- max(dataRange, na.rm=TRUE)

  if (percentage)
  {
    lower <- wt-(wt-minlim)*neutral_lower_percent
    upper <- wt+(maxlim-wt)*neutral_upper_percent
  }
  else
  {
    lower <- wt-neutral_lower_percent
    upper <- wt+neutral_upper_percent
  }

  sapply(values, function(x)
  {
    if(!is.na(x))
    {
      if(x < lower)
        x <- 1.5 - normalize_fixed_range_mapping(x,minlim,lower,.5,1)
      else if(x > upper)
        x <- normalize_fixed_range_mapping(x,upper,maxlim,.5,1)
      else
      {
        if(x < wt)
          x <- .5 - normalize_fixed_range_mapping(x,lower,wt,0,.5)
        else
          x <- normalize_fixed_range_mapping(x,wt,upper,0,.5)
      }
    }
    else
      x
  })
}

normalize1fold <- function(x)
{
  (x - min(x, na.rm=TRUE))/(max(x,na.rm=TRUE) - min(x, na.rm=TRUE))
}

normalize1foldfixed <- function(x, minv, maxv)
{
  (x - minv)/(maxv - minv)
}

normalize2fold <- function(threshold, values)
{
  sapply(values, function(x)
  {
    if (x < threshold)
    {
      (x - min(values, na.rm=TRUE))/(threshold - min(values, na.rm=TRUE))
    }
    else if (x > threshold)
    {
      (x - threshold)/(max(values,na.rm=TRUE) - threshold)
    }
    else (x)
  })
}

normalize2foldfixed <- function(threshold, minv, maxv, values)
{
  sapply(values, function(x)
  {
    if (x < threshold)
    {
      #(x - minv)/(threshold*2 - minv)
      ((x - minv)/(threshold - minv)) * (0.5 - 0) + 0
    }
    else if (x >= threshold)
    {
      #(x - threshold)/(maxv - 0.5)
      ((x - threshold)/(maxv - threshold)) * (1 - 0.5) + 0.5
    }
    #else (0.5)
  })
}
