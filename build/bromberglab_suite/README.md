# README #

### BrombergLab Suite ###

* BrombergLab Suite is a collection of mainly python and R scripts and packages for computational biology
* 0.1.dev0

### Setup ###

* Check out locally
* export PYTHONPATH=$PYTHONPATH:<absolute path to .../bromberglab_suite/python>
* export R_HOME=$R_HOME:<absolute path to R Resource, e.g. /Library/Frameworks/R.framework/Resources>

### Contribution guidelines ###

* Document your code!
* User friendly variable/function names
* More guidelines to come...

### Who do I talk to? ###

* Yannick Mahlich (ymahlich@bromberglab.org) and Maximilian Miller (mmiller@bromberglab.org)
* www.bromberglab.org