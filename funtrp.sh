#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

query_in="$1"
queryfile=$(basename -- "${query_in}")
extension="${queryfile##*.}"
queryid="${queryfile%.*}"

query_in_mounted="${DIR}/mnt/${queryfile}"
cp "${query_in}" "${query_in_mounted}"

echo "Computing input features (bromberglab/predictprotein) ..."
docker run --rm \
        -v "${DIR}/db":/usr/share/rostlab-data/data/big \
        -v "${DIR}/mnt":/mnt/local-storage \
        bromberglab/predictprotein predictprotein \
        --target=query.profRdb --target=query.mdisorder --target=consurf \
        --output-dir="/mnt/local-storage/pprotein/${queryid}" --work-dir="/mnt/local-storage/pprotein/${queryid}/tmp" \
        --seqfile="/mnt/local-storage/${queryfile}" \
        --num-cpus=2 \
        --blast-processors=10

echo "Running prediction (bromberglab/funtrp) ..."
docker run --rm \
        -v "${DIR}/db":/rheostat/db \
        -v "${DIR}/mnt":/mnt bromberglab/funtrp "${queryfile}"
